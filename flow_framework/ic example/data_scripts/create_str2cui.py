from zebra_tools import SimpleDatabaseIterator
import pickle

host = 'localhost'
# host = '62.61.146.181'
db_iter = SimpleDatabaseIterator(host, 'umls2015', 'guest', 'guest_dtu', 'select str, cui from mrconso group by str')
str2cui = {}
for row in db_iter:
    str2cui[row['str'].lower()] = row['cui'].lower()
pickle.dump(str2cui, open('str2cui.pkl', 'wb'))