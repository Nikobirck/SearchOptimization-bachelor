import pickle
import numpy as np
import os
import nltk


from flow_framework.base_generator import BaseGenerator


def encode(text, environment):
    for t in ([t[0] for t in environment['transformations'][0:-1]]):
        text = t(text, environment)
    return text

class Leave1OutGenerator(BaseGenerator):
    def __init__(self, cache_name):
        super(self.__class__, self).__init__()
        self.cache_name = cache_name


    def encode_text(self, text, token2id, text_size):
        encoded_text = [token2id[c] if c in token2id else 0 for c in text]
        # l = [c for c in text if c not in token2id]
        # if len(l) > 0:
        #     print(l)
        if len(encoded_text) < text_size:
            encoded_text += [0] * (text_size - len(encoded_text))
        assert len(encoded_text) == text_size
        encoded_text = np.asarray(encoded_text)
        return encoded_text

    def encoded_samples_gen(self, target_pair_type):
        target_cui2doc_id_symptom_name_pair = self.environment[target_pair_type]
        id2doc = self.environment['id2doc']
        cui2id = self.environment['class2id']
        token2id = self.environment['token2id']

        batch_size = int(self.environment['config']['sampling']['batch_size'])
        snippet_size = int(self.environment['config']['sampling']['snippet_size'])

        keys = list(target_cui2doc_id_symptom_name_pair.keys())
        weights = np.log(np.asarray([len(target_cui2doc_id_symptom_name_pair[w]) for w in keys], 'float32'))
        weights /= np.sum(weights)

        batch_classes = []
        batch_samples = []
        while True:
            key = np.random.choice(keys, 1, p=weights)[0]
            texts = target_cui2doc_id_symptom_name_pair[key]
            num_texts = len(texts)
            np.random.choice(keys, p=weights)
            doc_id, symptom_name_tok, start_indices = texts[np.random.randint(0,num_texts,1)[0]]

            snippet = leave1out_substring(id2doc[doc_id], symptom_name_tok, snippet_size, start_indices)


            encoded_snippet = self.encode_text(snippet, token2id, snippet_size)

            class_id = cui2id[key]

            batch_classes.append(class_id)
            batch_samples.append(encoded_snippet)
            if len(batch_classes) == batch_size:
                input_chars = np.vstack(batch_samples).astype('int32')
                batch_dict = {'input_words': input_chars}
                yield batch_dict, np.vstack(batch_classes).astype('int32')
                snippet_size = np.random.randint(4, 25,1).tolist()[0]
                batch_classes = []
                batch_samples = []

    def encoded_train_samples_gen(self):
        return self.encoded_samples_gen('target_cui2doc_id_symptom_name_pair_train')

    def encoded_valid_samples_gen(self):
        if int(self.environment['config']['data']['use_separate_validation_set']) == 1:
            return self.encoded_valid_samples_gen_from_file()
        else:
            return self.encoded_samples_gen('target_cui2doc_id_symptom_name_pair_valid')


    def encoded_valid_samples_gen_from_file(self):
        cache_dir = self.environment['config']['cache']['cache_dir']
        filename = os.path.join(cache_dir, self.cache_name, 'valid_samples.pkl')
        if os.path.isfile(filename):
            all_valid_samples = pickle.load(open(filename, 'rb'))
        else:
            cui2names = self.environment['cui2names']
            target_symptom_name2cui = {}
            max_ngram = 0
            for cui in cui2names:
                names = cui2names[cui]
                for name in names:
                    name = encode(name, self.environment)
                    max_ngram = max(len(name), max_ngram)
                    name = ' '.join(name)
                    target_symptom_name2cui[name] = cui

            all_valid_samples = []


            for valid_sample_num, sample in enumerate(self.environment['raw_valid_samples_gen']()):
                token_doc = sample['text']
                max_ngram = 5
                n_grams = sum([get_n_grams(token_doc, i) for i in range(1, max_ngram)],[])
                targets = list(set([n_gram for n_gram in n_grams if n_gram in target_symptom_name2cui]))
                if len(targets) == 0:
                    continue
                for target_name in targets:
                    # find start_indices and end indices for target
                    target_name_tok = nltk.word_tokenize(target_name)
                    candidate_start_indices = [i for i, t in enumerate(token_doc)
                                               if t == target_name_tok[0]]

                    candidate_start_indices = [i for i in candidate_start_indices
                                               if token_doc[i:(i+len(target_name_tok))] == target_name_tok]
                    for i in candidate_start_indices:
                        all_valid_samples.append((target_symptom_name2cui[target_name], token_doc[0:i]+token_doc[(i+len(target_name_tok))::]))
                        # print('Extracted %i validation samples' % len(all_valid_samples))
            pickle.dump(all_valid_samples, open(filename, 'wb'))
        print ('Found a total of %i validation samples' % len(all_valid_samples))

        token2id = self.environment['token2id']
        class2id = self.environment['class2id']



        all_valid_samples = [(cui, t) for cui, t in all_valid_samples]
        max_len = max([len(s) for cui, s in all_valid_samples])
        batch_classes = []
        batch_samples = []

        for (cui, text) in all_valid_samples:
            batch_samples.append(self.encode_text(text, token2id, max_len))
            batch_classes.append(class2id[cui])
        batch_samples = np.vstack(np.asarray(batch_samples, 'int32'))
        batch_classes = np.vstack(np.asarray(batch_classes, 'int32'))

        while True:
            yield batch_samples, batch_classes


    def encoded_test_samples_gen(self):
        cache_dir = self.environment['config']['cache']['cache_dir']
        filename = os.path.join(cache_dir, self.cache_name, 'test_samples.pkl')
        if os.path.isfile(filename):
            all_test_samples = pickle.load(open(filename, 'rb'))
        else:
            cui2names = self.environment['cui2names']
            target_symptom_name2cui = {}
            max_ngram = 0
            for cui in cui2names:
                names = cui2names[cui]
                for name in names:
                    name = encode(name, self.environment)
                    max_ngram = max(len(name), max_ngram)
                    name = ' '.join(name)
                    target_symptom_name2cui[name] = cui

            all_test_samples = []

            for i, sample in enumerate(self.environment['raw_test_samples_gen']()):
                token_doc = sample['text']
                max_ngram = 5
                n_grams = sum([get_n_grams(token_doc, i) for i in range(1, max_ngram)],[])
                targets = list(set([n_gram for n_gram in n_grams if n_gram in target_symptom_name2cui]))
                if len(targets) == 0:
                    continue
                for target_name in targets:
                    # find start_indices and end indices for target
                    target_name_tok = nltk.word_tokenize(target_name)
                    candidate_start_indices = [i for i, t in enumerate(token_doc)
                                               if t == target_name_tok[0]]

                    candidate_start_indices = [i for i in candidate_start_indices
                                               if token_doc[i:(i+len(target_name_tok))] == target_name_tok]
                    for i in candidate_start_indices:
                        all_test_samples.append((target_symptom_name2cui[target_name], token_doc[0:i]+token_doc[(i+len(target_name_tok))::]))
                        # print('Extracted %i test samples' % len(all_test_samples))
                pickle.dump(all_test_samples, open(filename, 'wb'))
        print ('Found a total of %i test samples' % len(all_test_samples))

        token2id = self.environment['token2id']
        class2id = self.environment['class2id']

        all_test_samples = [(cui, t) for cui, t in all_test_samples]
        max_len = max([len(s) for cui, s in all_test_samples])
        batch_classes = []
        batch_samples = []

        for (cui, text) in all_test_samples:
            batch_samples.append(self.encode_text(text, token2id, max_len))
            batch_classes.append(class2id[cui])
        batch_samples = np.vstack(np.asarray(batch_samples, 'int32'))
        batch_classes = np.vstack(np.asarray(batch_classes, 'int32'))

        while True:
            yield batch_samples, batch_classes


    @staticmethod
    def get_dependencies():
        return ['token2id', 'cui2prefered_name']

    @staticmethod
    def transform(text, env=None):
        token2id = env['token2id']
        return np.asarray([token2id[tok] if tok in token2id else 0 for tok in text ]).reshape((1, -1))



def get_n_grams(words, n):
    n_grams = []
    for i in range(0, len(words) - n + 1):
        n_grams.append(' '.join(words[i:(i + n)]))
    return n_grams

def leave1out_substring(text, symptom, snippet_size, start_indices):
    start_idx = start_indices[np.random.randint(0, len(start_indices))]
    num_tries = 1
    if len(text) > snippet_size:
        num_tries = 150
    for k in range(0,num_tries): # code this better!
        i = np.random.randint(0, min(snippet_size, start_idx)+1)
        tokens = text[(start_idx-i):start_idx] + text[(start_idx+len(symptom)):(start_idx+snippet_size+len(symptom)-i)]
        if len(tokens) == snippet_size:
            break
    return tokens




if __name__ == '__main__':
    from flow_framework import *
    from proposal_flow_components import LoadCui2NamesFlowComponent,LoadSymptomDicts, LoadClass2IdFlowComponent, SplitTrainTestFlowComponent

    flow = [ConfigLoaderFlowComponent('config.ini'),
            ExtendedFindzebraProvider(),
            RemovePunktFlowComponent('’–,|—“.-_”/"*\'=:'),
            LowercaseFlowComponent(),
            WordTokenizeFlowComponent(),
            CountWordsFlowComponent(),
            LoadToken2IdFlowComponent(),
            PruneInfrequentWords(min_token_count=5),

            # SetConfigValFlowComponent(['tokens', 'max_tokens'], 15000),
            # MaxWordsFlowComponent(),

            LoadCui2NamesFlowComponent(),  # add a cui2names and cui2prefered_name dictionaries into the environment
            LoadSymptomDicts(use_cache=True, max_num_targets=10000000000),
            # target_cui2doc_id_symptom_name_pair, id2doc (id -> document), token2count
            LoadClass2IdFlowComponent(),
            SplitTrainTestFlowComponent(valid_fraction=0.1),
            Leave1OutGenerator()
            ]
    env = FlowController(flow).execute()
    # for i, w in enumerate(env['encoded_train_samples_gen']()):
    for i, w in enumerate(env['encoded_valid_samples_gen']()):
        print (1)