import re, nltk
from flow_framework import *
from metrics import top_k_accuracy
import keras
from snippet_generator import SnippetGenerator
from search_flow_components import CreateCui2docMappingFlowComponent, LoadClass2IdFlowComponent, PrintNumTokensFlowComponent

if __name__ == '__main__':
    metrics = ['accuracy',top_k_accuracy(5),top_k_accuracy(10),top_k_accuracy(20)]
    loss = 'sparse_categorical_crossentropy'
    optimizer = keras.optimizers.Adam(lr=0.001, clipnorm=6.)

    model_name = 'configs.std_emb'
    max_words = 15000
    cache_name = 'final'

    config_spec = '%s_%i_%s' % (model_name, abs(max_words), cache_name)
    final_log = config_spec + '.txt'

    # weights_name = model_name
    checkpoint_path = 'checkpoints/{}'.format(config_spec)
    model_dir = 'final_models/{}'.format(config_spec)

    max_words_flow = [SetConfigValFlowComponent(['tokens', 'max_tokens'], max_words), MaxWordsFlowComponent()]
    max_words_flowcontroler = FlowController(max_words_flow)

    provider = FlowController([ConfigLoaderFlowComponent('config.ini'), ExtendedFindzebraProvider()])
    start_flow = [
        provider,
        RemovePunktFlowComponent('’–,|—“.-_”/"*\'=:'),
        LowercaseFlowComponent(),
        WordTokenizeFlowComponent(),
        CountWordsFlowComponent(cache_name),
        LoadToken2IdFlowComponent(cache_name),
        PruneInfrequentWords(min_token_count=5),
        max_words_flowcontroler,
        CreateCui2docMappingFlowComponent(),
        LoadClass2IdFlowComponent(cache_name),
        Word2VecFlowComponent(),
        TrainSamplesSnippetGeneratorsFlowComponent(),
        SnippetGenerator(),
        PrintNumTokensFlowComponent(),
        CreateModelFlowComponent(model_name),
        LoadBestWeightsFlowComponent(checkpoint_path)
        ]

    train_flow =  [
        CompileModelFlowComponent(loss=loss, metrics=metrics, optimizer=optimizer),
        AddModelSaveCallbackFlowComponent(checkpoint_path),
        AddPrintTopWordsCallbackFlowComponent(),
        AddLogProgressCallbackFlowComponent('logs', config_spec + '.csv'),
        AddEarlyStoppingCallbackFlowComponent(metric='val_loss', patience=10),
        AddCheckpointsCleanCallbackFlowComponent(checkpoint_path),
        PackageModelFiles(model_dir),
        TrainModelFlowComponent()
        ]



    flow = [
        FlowController(start_flow),
        FlowController(train_flow),
        LoadBestWeightsFlowComponent(checkpoint_path),
        LogModelTestPerformanceFlowComponent(max_n=20, log_dir='results', log_file=final_log),
        CopyBestWeightsFlowComponent(checkpoint_path, model_dir, 'best.hdf5')
        ]

    environment = FlowController(flow_components=flow).execute({})


