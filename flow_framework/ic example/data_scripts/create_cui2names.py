from zebra_tools.SimpleDatabaseIterator import *
from zebra_tools.disease_utils import *


umls_db_name = "umls2015"
user = 'root'
password = 'findzebra'
umls_db_host = "localhost"
umls_util = UMLS_util(umls_db_host, umls_db_name, user, password)

removed_words = open('removed_words.txt').read().split('\n')
removed_words = dict([(w,None) for w in removed_words])

cui_targets = open('curated_cuis.txt').read().split('\n')
cui_targets = ["'" + cui + "'" for cui in cui_targets]
sql = 'select str, cui from mrconso where cui in (%s)' % ",".join(cui_targets)
iter = SimpleDatabaseIterator('localhost', 'umls2015','root', 'findzebra',sql)
cui2names = {}
cui2preferedname = {}
for row in iter:
    cui = row['cui'].lower()
    name = row['str'].lower()
    if name in removed_words:
        continue
    cui2names.setdefault(cui, [])
    cui2names[cui].append(name)
    if cui not in cui2preferedname:
        cui2preferedname[cui] = name

import pickle

pickle.dump(cui2names, open('cui2english_names.pkl', 'wb'))
pickle.dump(cui2preferedname, open('cui2prefered_english_name.pkl', 'wb'))







