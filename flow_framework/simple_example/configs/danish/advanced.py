import keras.backend as K
from keras.models import Model, Sequential
from keras.layers import Dense, Activation, TimeDistributed, Dropout, GRU, Lambda, Merge, Permute, Reshape, Flatten
from keras.layers import BatchNormalization, Input, merge
from keras.regularizers import l2
from layers import Attention, MultiHashingLayerDropout, ReduceSum, TemporalDropOut, CosineLossLayer
import pickle
import numpy as np
from flow_framework import AbstractConfig
from flow_framework import *

class Config(AbstractConfig):
    reg_factor   = 0.000000  # regularization factor lambda
    base_size = 64
    softmax_size = 128
    aggregation_function = 'sum'
    # aggregation_function = 'concatenate'
    num_buckets = 2000
    embedding_size = 150
    num_hashes = 5
    p_init_std = 0.0005


    def create_model(self, environment):
        self.num_classes = max(environment['token2id'].values())+1
        num_words = max(environment['token2id'].values())+1 # num_words

        self.reg_factor_p = 1.0/(num_words*self.num_hashes*self.p_init_std)

        W = np.random.normal(0, 0.1, (self.num_buckets, self.embedding_size))
        input_words = Input([None],dtype='int32', name='input_words')

        p_init = np.random.normal(0, self.p_init_std, (num_words, self.num_hashes))

        m = MultiHashingLayerDropout(W=W, word_count_including_zero_token=num_words, p_init=p_init, aggregation_mode=self.aggregation_function,
                                     W_trainable=True, p_trainable=True, append_weight=True, mask_zero=True, reg_factor=self.reg_factor_p)
        x = m(input_words)
        x = Dropout(0.5)(x)
        # x = TemporalDropOut(0.1)(x)
        x = BatchNormalization()(x)

        x = TimeDistributed(Dense(self.base_size*2, activation='tanh'))(x)
        x = BatchNormalization()(x)

        x = GRU(200,return_sequences=False)(x)

        x = Dense(self.softmax_size,activation='relu')(x)
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)

        x = Dense(self.softmax_size,activation='relu')(x)
        # x = merge([x, res], 'sum')
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)

        x = Dense(self.softmax_size, W_regularizer=l2(self.reg_factor))(x)

        x = BatchNormalization()(x)
        # x = Dropout(0.5)(x)

        x = Activation('tanh')(x)


        output_word = Input([1],dtype='int32', name='output_word')

        m_out = MultiHashingLayerDropout(W=W, word_count_including_zero_token=num_words, p_init=p_init,
                                     aggregation_mode=self.aggregation_function,
                                     W_trainable=True, p_trainable=True, append_weight=True, mask_zero=False,
                                     reg_factor=self.reg_factor_p)

        # f = K.function([output_word, K.learning_phase(), input_words], [loss])
        # in_p = [np.asarray([[1],[2]], 'int32'), 0,np.asarray([[1,2,3], [1,2,3]], 'int32')]
        # f(in_p)[0]
        output_word_vec = m_out(output_word)
        output_word_vec = Flatten()(output_word_vec)
        output_word_vec = BatchNormalization()(output_word_vec)
        output_word_vec = Dense(self.softmax_size,activation='relu')(output_word_vec)
        output_word_vec = BatchNormalization()(output_word_vec)
        output_word_vec = Dense(self.softmax_size,activation='tanh')(output_word_vec)
        # output_word_vec = Reshape([self.softmax_size])(output_word_vec)

        # loss = merge([x, output_word_vec],mode='cos', name='output')
        loss = CosineLossLayer(name='output')([x, output_word_vec])
        loss = Reshape([1])(loss)
        environment['f'] = K.function([output_word, K.learning_phase(), input_words], [output_word_vec, x, loss])


        environment['model'] = Model(input=[input_words, output_word], output=loss)
        environment['multi_hashing_layer'] = m

        return environment
