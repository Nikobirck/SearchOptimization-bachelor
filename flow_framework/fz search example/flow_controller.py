import re, nltk
from flow_framework import *
from metrics import top_k_accuracy
import keras
from snippet_generator import SnippetGenerator
from search_flow_components import CreateCui2docMappingFlowComponent, LoadClass2IdFlowComponent, PrintNumTokensFlowComponent




if __name__ == '__main__':
    metrics = ['accuracy',top_k_accuracy(5),top_k_accuracy(10),top_k_accuracy(20)]
    loss = 'sparse_categorical_crossentropy'
    optimizer = keras.optimizers.Adam(lr=0.001, clipnorm=6.)


    models = []

    for token_type in ['unigram', 'n_gram']:
        for word_or_bucket_count in [1000, 2500, 5000, 10000, 15000, 20000]:
            models.append(('configs.english.hash_emb', -1, token_type, word_or_bucket_count))
            models.append(('configs.english.std_emb', word_or_bucket_count, token_type, -1))

    for model_name, max_words, cache_name, word_or_bucket_count in models:
        # model_name, max_words, cache_name, word_or_bucket_count = ('configs.english.hash_emb', -1, 'unigram', 2500)

        provider = FlowController([ConfigLoaderFlowComponent('config.ini'), ExtendedFindzebraProvider()])

        config_spec = '%s_%i_%s_%i' % (model_name, abs(max_words), cache_name, abs(word_or_bucket_count))
        final_log = config_spec + '.txt'
        if os.path.isfile(os.path.join('results', final_log)):
            continue

        # weights_name = model_name
        checkpoint_path = 'checkpoints/{}'.format(config_spec)
        model_dir = 'final_models/{}'.format(config_spec)

        num_buckets_flow = []
        if word_or_bucket_count > 0:
            num_buckets_flow = [SetConfigValFlowComponent(['model', 'num_buckets'], word_or_bucket_count)]
        num_buckets_flow_controller = FlowController(num_buckets_flow)


        max_words_flow = []
        if max_words > 0:
            max_words_flow = [SetConfigValFlowComponent(['tokens', 'max_tokens'],max_words), MaxWordsFlowComponent()]
        max_words_flowcontroler = FlowController(max_words_flow)

        ngram_flow = []
        if not (cache_name == 'unigram'):
            ngram_flow = [NGramFlowComponent('bigram.pkl', cache_name), NGramFlowComponent('trigram.pkl', cache_name)]
        ngram_flowcontroler = FlowController(ngram_flow)

        start_flow = [
            provider,
            RemovePunktFlowComponent('’–,|—“.-_”/"*\'=:'),
            LowercaseFlowComponent(),
            WordTokenizeFlowComponent(),
            num_buckets_flow_controller,
            ngram_flowcontroler,
            CountWordsFlowComponent(cache_name),
            LoadToken2IdFlowComponent(cache_name),
            PruneInfrequentWords(min_token_count=5),
            max_words_flowcontroler,
            CreateCui2docMappingFlowComponent(),
            LoadClass2IdFlowComponent(cache_name),
            TrainSamplesSnippetGeneratorsFlowComponent(),
            SnippetGenerator(),
            PrintNumTokensFlowComponent(),
            CreateModelFlowComponent(model_name),
            LoadBestWeightsFlowComponent(checkpoint_path)
            ]

        train_flow =  [
            CompileModelFlowComponent(loss=loss, metrics=metrics, optimizer=optimizer),
            AddModelSaveCallbackFlowComponent(checkpoint_path),
            AddPrintTopWordsCallbackFlowComponent(),
            AddLogProgressCallbackFlowComponent('logs', config_spec + '.csv'),
            AddEarlyStoppingCallbackFlowComponent(metric='val_loss', patience=10),
            AddCheckpointsCleanCallbackFlowComponent(checkpoint_path),
            PackageModelFiles(model_dir, model_name),
            TrainModelFlowComponent(),
            LoadBestWeightsFlowComponent(checkpoint_path),
            LogModelTestPerformanceFlowComponent(max_n=20, log_dir='results', log_file=final_log),
            # CopyBestWeightsFlowComponent(checkpoint_path, model_dir, 'best.hdf5')
            ]



        flow = [
            FlowController(start_flow),
            FlowController(train_flow),
            LogModelTestPerformanceFlowComponent(max_n=20, log_dir='results', log_file=final_log)
            ]

        environment = FlowController(flow_components=flow).execute({})
        environment = None


