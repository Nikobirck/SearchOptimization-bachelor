from zebra_tools import SimpleDatabaseIterator
import pickle

disease_stopwords = ['wound', 'lip', 'giant', 'cord', 'defect', 'soft', 'abscess', 'air','bite', 'bleeding', 'can',
                     'was', 'cas', 'cavity', 'dislocation', 'face', 'foreign body', 'fracture', 'infection', 'rash', 'scar', 'secondary']

sql = 'SELECT str, umls_concept_id as cui FROM findzebra2.page p, umls2015.mrconso con where con.cui = p.umls_concept_id'

host = 'localhost'
fz_str2cui = {}
# host = '62.61.146.181'
db_iter = SimpleDatabaseIterator(host, 'findzebra2', 'guest', 'guest_dtu', sql)
for row in db_iter:
    disease_name = row['str'].lower()
    if len(disease_name) < 3 or disease_name in disease_stopwords:
        continue
    fz_str2cui[disease_name] = row['cui'].lower()


pickle.dump(fz_str2cui, open('fz_str2cui.pkl', 'wb'))