from flow_framework.base_generator import BaseGenerator
import numpy as np

def random_substring(text, num_words):
    n = len(text)
    if n <= num_words:
        return text
    start_idx = np.random.randint(0, n - num_words)
    return text[start_idx:(start_idx + num_words)]


class PredictNextGenerator(BaseGenerator):
    def encode_text(self, text, token2id, text_size):
        encoded_text = [token2id[c] if c in token2id else 0 for c in text]
        if len(encoded_text) < text_size:
            encoded_text += [0] * (text_size - len(encoded_text))
        assert len(encoded_text) == text_size
        encoded_text = np.asarray(encoded_text)
        return encoded_text

    def encoded_train_samples_gen(self):
        batch_classes = []
        batch_samples = []
        token2id = self.environment['token2id']
        batch_size = int(self.environment['config']['sampling']['batch_size'])
        snippet_size = int(self.environment['config']['sampling']['snippet_size'])
        max_len = snippet_size
        while True:
            for d in self.environment['raw_train_samples_gen']():
                substr = random_substring(d['text'], max_len)
                source = substr[0:-1]
                target = substr[-1]
                if target not in token2id:
                    continue
                batch_samples.append(self.encode_text(source, token2id, max_len))
                batch_classes.append(token2id[target])# += [token2id[t] for t in target]

                if len (batch_samples) == batch_size:
                    batch_samples = np.vstack(np.asarray(batch_samples, 'int32'))
                    batch_classes = np.vstack(np.asarray(batch_classes, 'int32'))
                    yield batch_samples, batch_classes
                    batch_classes = []
                    batch_samples = []


    def encoded_valid_samples_gen(self):
        batch_classes = []
        batch_samples = []
        token2id = self.environment['token2id']
        max_len = 50
        for d in self.environment['raw_valid_samples_gen']():
            substr = random_substring(d['text'], max_len)
            source = substr[0:-1]
            # target = substr[1::]
            target = substr[-1]
            if target not in token2id:
                continue
            batch_samples.append(self.encode_text(source, token2id, max_len))
            batch_classes += [token2id[target]]

        batch_samples = np.vstack(np.asarray(batch_samples, 'int32'))
        batch_classes = np.vstack(np.asarray(batch_classes, 'int32'))
        while True:
            yield batch_samples, batch_classes

    @staticmethod
    def get_dependencies():
        return ['token2id']

    @staticmethod
    def transform(text, env=None):
        token2id = env['token2id']
        return np.asarray([token2id[tok] if tok in token2id else 0 for tok in text]).reshape((1, -1))


if __name__ == '__main__':
    from flow_framework import *
    replacement_chars = '’–,|—“.-_”/"*\'=:'

    flow = [
        FlowController([ConfigLoaderFlowComponent('config_dk.ini'), DanishCorpusProvider()]),
        RemovePunktFlowComponent(replacement_chars),
        LowercaseFlowComponent(),
        WordTokenizeFlowComponent(),
        ]

    environment = FlowController(flow_components=flow).execute()



    for d in environment['raw_train_samples_gen']():
        print (random_substring(d['text'], 15))
        print (random_substring(d['text'], 15))
