# Search Optimization bachelor thesis

By Nikolaj Birck Frederiksen s130184

- The Python libraries is specified the the requirement.txt file.
- You will find the work I have done under flow_framework/searchOptimization.
- The flow_controller.py is the main file, which controls the flow from data processing to training the model.
- You can find the model under final_models/configs.std_emb_final.
- The results folder contains a file which specifices the accuracy from 1-20 answers.