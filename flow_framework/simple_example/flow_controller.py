from metrics import top_k_accuracy
import keras
from flow_framework import *
# from predict_next_generator import PredictNextGenerator
from predict_next_generator_advanced import PredictNextGenerator

class SetClass2IdFlowComponent(AbstractFlowComponent):
    def execute(self, environment={}):
        environment['class2id'] = environment['token2id']
        return environment

class TestFlowComponent(AbstractFlowComponent):
    def execute(self, environment={}):
        f = environment['f']

        for d in environment['encoded_train_samples_gen']():
            for i in range(0,d[0]['output_word'].shape[0]):
                out_word = d[0]['output_word'][i].reshape((1,-1))
                input_words_ = d[0]['input_words'][i].reshape((1,-1))
                output_word_vec_, x_, loss_ = f([out_word, 0,input_words_])
                x_ = np.asarray([1, 2, 3,4])
                output_word_vec_ = np.asarray([-1, -2, -3,-4])
                norm_x = np.sqrt(np.sum(x_**2))
                norm_output_word_vec_= np.sqrt(np.sum(output_word_vec_**2))
                loss_calc = np.sum((output_word_vec_ * x_/(norm_output_word_vec_*norm_x)))
        return environment


if __name__ == '__main__':
    model_dir = 'model_files'
    model_name = 'configs.danish.simple'
    model_name = 'configs.danish.advanced'
    weights_name = model_name
    checkpoint_path = 'checkpoints/{}'.format(weights_name)

    metrics = ['accuracy',top_k_accuracy(5),top_k_accuracy(10),top_k_accuracy(20)]
    loss = 'sparse_categorical_crossentropy'
    optimizer = keras.optimizers.Adam(lr=0.001, clipnorm=6.)



    replacement_chars = '’–,|—“.-_”/"*\'=:'

    flow = [
        FlowController([ConfigLoaderFlowComponent('config_dk.ini'), ExtendedFindzebraProvider()]),
        RemovePunktFlowComponent(replacement_chars),
        LowercaseFlowComponent(),
        WordTokenizeFlowComponent(),
        CountWordsFlowComponent(),
        LoadToken2idFlowComponent(2),
        SetClass2IdFlowComponent(),
        PredictNextGenerator(),
        CreateModelFlowComponent(model_name),
        LoadBestWeightsFlowComponent(checkpoint_path),
        # TestFlowComponent(),
        # CompileModelFlowComponent(loss=loss, metrics=metrics, optimizer=optimizer),
        CompileModelFlowComponent(loss='mean_absolute_error', metrics=['mean_absolute_error'], optimizer=optimizer),
        AddModelSaveCallbackFlowComponent(checkpoint_path),
        AddPrintTopWordsCallbackFlowComponent(),
        # AddLogProgressCallbackFlowComponent('logs', 'out.csv'),
        AddEarlyStoppingCallbackFlowComponent(metric='val_loss', patience=30),
        PackageModelFiles(model_dir, model_name),
        TrainModelFlowComponent(),
        CopyBestWeightsFlowComponent(checkpoint_path, model_dir, 'best.hdf5')
        ]

    environment = FlowController(flow_components=flow).execute()


