import re, nltk
from flow_framework import *
from metrics import top_k_accuracy
import keras
from snippet_generator import SnippetGenerator
from search_flow_components import CreateCui2docMappingFlowComponent, LoadClass2IdFlowComponent, PrintNumTokensFlowComponent, BordingJoinSummaryAndTextFlowComponent




if __name__ == '__main__':



    metrics = ['accuracy',top_k_accuracy(5),top_k_accuracy(10),top_k_accuracy(20)]
    loss = 'sparse_categorical_crossentropy'
    optimizer = keras.optimizers.Adam(lr=0.001, clipnorm=6.)


    models = []

    # for token_type in ['unigram', 'n_gram']:
    #     for word_or_bucket_count in [1000, 2500, 5000, 10000, 15000, 20000]:
    #         models.append(('configs.english.hash_emb', -1, token_type, word_or_bucket_count))
    #         models.append(('configs.english.std_emb', word_or_bucket_count, token_type, -1))

    models.append( ('configs.english.hash_emb_dropout', 2500, 'char', 20000))

    for model_name, max_words, cache_name, word_or_bucket_count in models:
        # provider = FlowController([ConfigLoaderFlowComponent('config.ini'), ExtendedFindzebraProvider()])
        provider = FlowController([ConfigLoaderFlowComponent('bording_config.ini'), BordingProvider()])

        config_spec = '%s_%i_%s_%i' % (model_name, abs(max_words), cache_name, abs(word_or_bucket_count))
        final_log = config_spec + '.txt'
        if os.path.isfile(os.path.join('results', final_log)):
            continue

        # weights_name = model_name
        checkpoint_path = 'checkpoints/{}'.format(config_spec)
        model_dir = 'final_models/{}'.format(config_spec)

        num_buckets_flow = []
        if word_or_bucket_count > 0:
            num_buckets_flow = [SetConfigValFlowComponent(['model', 'num_buckets'], word_or_bucket_count)]
        num_buckets_flow_controller = FlowController(num_buckets_flow)


        max_words_flow = []
        if max_words > 0:
            max_words_flow = [SetConfigValFlowComponent(['tokens', 'max_tokens'],max_words), MaxWordsFlowComponent()]
        max_words_flowcontroler = FlowController(max_words_flow)

        ngram_flow = []
        if not (cache_name == 'unigram'):
            ngram_flow = [NGramFlowComponent('bigram.pkl', cache_name, phrases_args={'threshold':0.0001}), NGramFlowComponent('trigram.pkl', cache_name, {'threshold':0.001}), NGramFlowComponent('tetragram.pkl', cache_name)]#, NGramFlowComponent('pentagram.pkl', cache_name)]
        ngram_flowcontroler = FlowController(ngram_flow)

        start_flow = [
            provider,
            BordingJoinSummaryAndTextFlowComponent(),
            # RemovePunktFlowComponent('’–,|—“.-_”/"*\'=:'),
            LowercaseFlowComponent(),
            CharTokenizeFlowComponent(),
            num_buckets_flow_controller,
            ngram_flowcontroler,
            # max_words_flowcontroler
        ]
        flow = [
            FlowController(start_flow),
            ]

        environment = FlowController(flow_components=flow).execute({})
        corpus = []
        for i, w in enumerate(environment['raw_train_samples_gen']()):
            corpus.append(w['text'])
            if i > 100:
                break
            print(w['text'])

        # u = Phrases(sentences=corpus, threshold=1)
        # for c in [l for l in (list(u.vocab.keys())) if len(l) == 1]:
        #     u.vocab[c] = max(1,int(u.vocab[c] // 100))
        # s = [str(c)for c in 'disease']
        # print(list(u[s]))
        # import nltk
        # nltk.bigrams(corpus)
        # environment = None


