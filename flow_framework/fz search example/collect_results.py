import os
import csv

rows = []
fields = ['valid_cost', 'model_name', 'max_words', 'token_type', 'bucket_count'] + ['recall@%i' % i for i in range(1, 21)]
for file in os.listdir('results'):
    file = file.replace('n_gram', 'ngram') # fix bad token type
    spec = file.split('_')
    max_words, token_type, bucket_count = spec[-3::]
    max_words = int(max_words)
    bucket_count = int(bucket_count[0:-4])
    model_name = '.'.join(spec[0:-3])
    results = open(os.path.join('results', file), 'r', encoding='utf-8').read().split('\t')
    results = [float(r) for r in results]
    valid_cost = float(os.listdir(os.path.join('checkpoints', file[0:-4]))[0][9:18])

    row = {'valid_cost':valid_cost, 'model_name':model_name, 'max_words':max_words, 'token_type':token_type, 'bucket_count':bucket_count}
    for i in range(1, 21):
        row[('recall@%i' % i)] = results[i-1]
    rows.append(row)

writer = csv.DictWriter(open('all_results.txt', 'w', encoding='utf-8'),fieldnames=fields, lineterminator='\n', delimiter=',')
writer.writeheader()
writer.writerows(rows)
