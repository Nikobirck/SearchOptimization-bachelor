from flow_framework import *
# from gensim.models import Phrases
from gensim.models.phrases import Phrases
import gensim
from proposal_flow_components import *

if __name__ == '__main__':
    replacement_chars = '’–,|—“.-_”/"*\'=:'
    flow = [
        FlowController([ConfigLoaderFlowComponent('config.ini'), ExtendedFindzebraProvider()]),
        RemovePunktFlowComponent(replacement_chars),
        LowercaseFlowComponent(),
        WordTokenizeFlowComponent(),

        ]

    environment = FlowController(flow_components=flow).execute()

    texts = []
    for c, row in enumerate(environment['raw_train_samples_gen']()):
        texts.append(row['text'])
        if c > 10:
            break

    import logging
    import copy
    class test():
        def __init__(self, vocab):
            self.vocab = copy.deepcopy(vocab)

        def __iter__(self):
            return self.vocab.__iter__()


        def __getitem__(self, item):
            if item == b'go_terms':
                return 10000
            else:
                return self.vocab[item]

        def __len__(self):
            return self.vocab.__len__()

    logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
    bigram = Phrases(texts)
    bigram.vocab = test(bigram.vocab)
    # print([(w, bigram.vocab[w]) for w in bigram.vocab if bigram.vocab[w] > 100 ])
    print(bigram[['increased', 'night', 'vision', 'in', 'gene', 'id', 'go', 'terms']])
    bigram.vocab[b'go_terms']
    bigram.vocab.__delitem__(b'go_terms')
    print(bigram[['increased', 'night', 'vision', 'in', 'gene', 'id', 'go', 'terms']])

