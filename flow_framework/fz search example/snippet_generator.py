import numpy as np

from flow_framework import BaseGenerator




class SnippetGenerator(BaseGenerator):

    def encode_text(self, text, token2id, text_size):
        # encoded_text = [token2id[c] if c in token2id else 0 for c in text]
        encoded_text = [token2id[c] if c in token2id else 0 for c in text]
        if len(encoded_text) < text_size:
            encoded_text += [0] * (text_size - len(encoded_text))
        assert len(encoded_text) == text_size
        encoded_text = np.asarray(encoded_text)
        return encoded_text

    def encoded_train_samples_gen(self):
        # this method must be a generator of valid samples of the form (class_id, encoded text). The raw_train_samples_gen
        # can be of either raw text or list of words.

        batch_size = int(self.environment['config']['sampling']['batch_size'])
        snippet_size = int(self.environment['config']['sampling']['snippet_size'])
        token2id = self.environment['token2id']
        class2id = self.environment['class2id']
        snippet_generator = self.environment['raw_snippet_gen']


        batch_classes = []
        batch_samples = []
        while True:
            snippet, class_id = snippet_generator.create_snippet(snippet_size)
            class_id = class2id[class_id]

            encoded_snippet = self.encode_text(snippet, token2id, snippet_size)

            batch_classes.append(class_id)
            batch_samples.append(encoded_snippet)
            if len(batch_classes) == batch_size:
                input_words = np.vstack(batch_samples).astype('int32')
                batch_dict = {'input_words': input_words}
                yield batch_dict, np.vstack(batch_classes).astype('int32')
                snippet_size = np.random.randint(4, 25,1).tolist()[0]
                batch_classes = []
                batch_samples = []


    def encoded_valid_samples_gen(self):
        # this method must be a generator of valid samples of the form (class_id, encoded text). The raw_valid_samples_gen
        # can be of either raw text or list of words.
        token2id = self.environment['token2id']
        class2id = self.environment['class2id']

        batch_classes = []
        batch_samples = []


        max_len = 0
        for sample in self.environment['raw_valid_samples_gen']():
            (class_name, text) = (sample['class_'], sample['text'])
            max_len = max(max_len, len(text))
        for sample in self.environment['raw_valid_samples_gen']():
            (class_name, text) = (sample['class_'], sample['text'])
            encoded_text = self.encode_text(text, token2id, max_len)


            assert len(encoded_text) == max_len
            if class_name not in class2id:
                print ('Have not seen cui %s before, skipping' % class_name)
                continue
            class_id = class2id[class_name]

            batch_classes.append(class_id)
            batch_samples.append(encoded_text)

        input_words = np.vstack(batch_samples).astype('int32')
        batch_dict = {'input_words': input_words}
        targets = np.vstack(batch_classes).astype('int32')
        num_samples_in_batch = 50
        while True:
            for i in range(0, len(targets), num_samples_in_batch):
                yield batch_dict['input_words'][i:(i+num_samples_in_batch),::], targets[i:(i+num_samples_in_batch)]


    def encoded_test_samples_gen(self):
        # this method must be a generator of valid samples of the form (class_id, encoded text). The raw_valid_samples_gen
        # can be of either raw text or list of words.
        token2id = self.environment['token2id']
        class2id = self.environment['class2id']

        batch_classes = []
        batch_samples = []

        max_len = 0
        for sample in self.environment['raw_test_samples_gen']():
            (class_name, text) = (sample['class_'], sample['text'])
            max_len = max(max_len, len(text))
        for sample in self.environment['raw_test_samples_gen']():
            (class_name, text) = (sample['class_'], sample['text'])
            encoded_text = self.encode_text(text, token2id, max_len)

            assert len(encoded_text) == max_len
            # class_name = class_name.upper()
            if class_name not in class2id:
                print ('Have not seen cui %s before, skipping' % class_name)
                continue
            class_id = class2id[class_name]

            batch_classes.append(class_id)
            batch_samples.append(encoded_text)

        input_words = np.vstack(batch_samples).astype('int32')
        batch_dict = {'input_words': input_words}
        targets = np.vstack(batch_classes).astype('int32')
        while True:
            yield batch_dict, targets
        # num_samples_in_batch = 50
        # while True:
        #     for i in range(0, len(targets), num_samples_in_batch):
        #         yield batch_dict['input_words'][i:(i + num_samples_in_batch), ::], targets[i:(i + num_samples_in_batch)]


    @staticmethod
    def get_dependencies():
        return ['token2id']


    @staticmethod
    def transform(text, env=None):
        token2id = env['token2id']
        return np.asarray([token2id[tok] if tok in token2id else 0 for tok in text]).reshape((1, -1))




if __name__ == '__main__':
    from flow_framework import ConfigLoaderFlowComponent, TokenizeWordsFlowComponent, FlowController, BordingProvider
    env = FlowController([ConfigLoaderFlowComponent('config.ini'), BordingProvider(), TokenizeWordsFlowComponent(), SnippetGenerator()]).execute()
    for w in env['encoded_test_samples_gen']():
        print (w.shape)