from keras import backend as K

def top_k_accuracy(k):
    def top_k(y_true, y_pred):
        if K.backend() == 'theano':
            import theano.tensor as T
            import theano
            highest_probs_idx = T.argsort(y_pred, axis=1)[:, -k:]
            y_true_mat = T.repeat(y_true,k,axis=1)
            is_hit = T.eq(highest_probs_idx, y_true_mat)
            accuracy = T.mean(T.any(is_hit, axis=1), dtype=theano.config.floatX)
            return accuracy
        else:
            import tensorflow as tf
            hits = tf.nn.in_top_k(y_pred, tf.to_int32(tf.squeeze(y_true,[1])), k)
            return tf.reduce_mean(tf.to_float(hits))
    top_k.__name__ = 'top {}'.format(k)
    return top_k

