import nltk
import pandas as pd
import os
import pickle
from flow_framework.base_provider import BaseProvider
from zipfile import ZipFile
import csv
import lxml.html as lh
from lxml.html.clean import clean_html
import re

csv.field_size_limit(10**9)

class BordingProvider(BaseProvider):
    def raw_train_samples_gen(self):
        for row_num, sample in self.corpus.iterrows():
            sample['class_'] = sample['cui'].lower()
            yield sample

    def raw_valid_samples_gen(self):
        for row_num, sample in self.val_set.iterrows():
            sample['class_'] = sample['cui'].lower()
            yield sample

    def raw_test_samples_gen(self):
        for cui, text in self.test_samples:
            sample = {'class_': cui.lower(), 'text': text}
            yield sample

    def prepare(self):
        if not os.path.exists(self.environment['config']['data']['data_dir']):
            import urllib.request
            print("The required data isn't present...")
            print('Creating data directory')
            os.makedirs('data/bording')
            print('downloading training data (~140 MB)')
            urllib.request.urlretrieve('https://www.dropbox.com/s/ty8bwft5ttpuyzx/corpus.csv?dl=1', os.path.join(self.environment['config']['data']['data_dir'], 'corpus.csv'))
            print('downloading other data')
            urllib.request.urlretrieve('https://www.dropbox.com/s/03lzqdhkn6abfw1/cui2idx.csv?dl=1', os.path.join(self.environment['config']['data']['data_dir'], 'cui2idx.csv'))
            urllib.request.urlretrieve('https://www.dropbox.com/s/7kzwrbspdvekhwy/validation_queries.csv?dl=1', os.path.join(self.environment['config']['data']['data_dir'], 'validation_queries.csv'))
            print('downloading test queries')
            urllib.request.urlretrieve('http://www.intellifind.dk/doctors_dilemma/test_queries.csv',
                                       os.path.join(self.environment['config']['data']['data_dir'],
                                                    'test_queries.csv'))

            print('Done')

    def load_data(self):
        self.environment['class2id'] = pd.read_csv(os.path.join(self.environment['config']['data']['data_dir'], 'cui2idx.csv'), index_col=0,encoding='utf-8').to_dict()['0']
        self.val_set = pd.read_csv(os.path.join(self.environment['config']['data']['data_dir'], 'validation_queries.csv'),encoding='utf-8')
        self.corpus = pd.read_csv(os.path.join(self.environment['config']['data']['data_dir'], 'corpus.csv'),encoding='utf-8')

        self.environment['num_validation_samples'] = len(self.val_set)

        with open(os.path.join(self.environment['config']['data']['data_dir'], 'test_queries.csv'), 'r', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            self.test_samples = [(row[2].lower(), row[1]) for row in reader][1::]
            self.environment['num_test_samples'] = len(self.test_samples)



"""
Raw data iterator for the findzebra dataset
"""
class ExtendedFindzebraProvider(BaseProvider):
    def raw_train_samples_gen(self):
        # Load the raw data as a list of tuples
        print(' - loading data')
        with ZipFile(os.path.join(self.environment['config']['data']['data_dir'], 'findzebra_web_raw.zip'), 'r') as ziparchive:
            for count, filename in enumerate(ziparchive.namelist()[1:]):  # don't include the containing folder as file
                # if count > 5000:
                #     break
                with ziparchive.open(filename, 'r') as f:
                    class_, text = tuple(f.read().decode('utf8').split(' |||| '))
                    sample = {'class_': class_, 'text':text}
                    yield sample

    def raw_valid_samples_gen(self):
        for cui, text in self.valid_samples:
            sample = {'class_': cui, 'text': text}
            yield sample

    def raw_test_samples_gen(self):
        for cui, text in self.test_samples:
            sample = {'class_': cui, 'text': text}
            yield sample

    def prepare(self):
        if not os.path.exists(self.environment['config']['data']['data_dir']):
            import urllib.request
            print('creating data directory: %s' % self.environment['config']['data']['data_dir'])
            os.makedirs(self.environment['config']['data']['data_dir'])
            print('downloading training data (~168 MB)')
            urllib.request.urlretrieve('https://www.dropbox.com/s/085ggy6099y5zfc/findzebra_web_raw.zip?dl=1',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'findzebra_web_raw.zip'))
            print('downloading validation data')
            urllib.request.urlretrieve('https://www.dropbox.com/s/hu9oc9z5l4wqnuc/doctors_dilemma_valid.csv?dl=1',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'doctors_dilemma_valid.csv'))

            print('downloading test queries')
            urllib.request.urlretrieve('http://www.intellifind.dk/doctors_dilemma/test_queries.csv',
                                       os.path.join(self.environment['config']['data']['data_dir'],
                                                    'test_queries.csv'))

            print ('Data download complete')

    def load_data(self):
        with open(os.path.join(self.environment['config']['data']['data_dir'], 'doctors_dilemma_valid.csv'), 'r', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            self.valid_samples = [(row[1], row[0]) for row in reader][1::]
            self.environment['num_validation_samples'] = len(self.valid_samples)

        with open(os.path.join(self.environment['config']['data']['data_dir'], 'test_queries.csv'), 'r', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            self.test_samples = [(row[2].lower(), row[1]) for row in reader][1::]
            self.environment['num_test_samples'] = len(self.test_samples)

def get_n_grams(words, n):
    n_grams = []
    for i in range(0, len(words) - n + 1):
        n_grams.append(' '.join(words[i:(i + n)]))
    return n_grams
"""
Raw data iterator for the pubmed dataset
"""
class PubmedProvider(BaseProvider):
    def raw_train_samples_gen(self):
        for sample_num in self.text_num2text_and_abstract:
            yield self.text_num2text_and_abstract[sample_num]

    def raw_valid_samples_gen(self):
        for cui, text in self.valid_samples:
            sample = {'class_': cui, 'text': text}
            yield sample

    def raw_test_samples_gen(self):
        for cui, text in self.test_samples:
            sample = {'class_': cui, 'text': text}
            yield sample

    def prepare(self):
        if not os.path.isfile(os.path.join(self.environment['config']['data']['data_dir'], 'text.zip')):
            import urllib.request
            if not os.path.isdir(self.environment['config']['data']['data_dir']):
                print('creating data directory: %s' % self.environment['config']['data']['data_dir'])
                os.makedirs(self.environment['config']['data']['data_dir'])
            print('downloading training data (~230 MB)')
            urllib.request.urlretrieve('http://www.intellifind.dk/pubmed/text.zip',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'text.zip'))
            urllib.request.urlretrieve('http://www.intellifind.dk/pubmed/abstracts.zip',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'abstracts.zip'))
            urllib.request.urlretrieve('http://www.intellifind.dk/pubmed/fz_str2cui.pkl',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'fz_str2cui.pkl'))
            print('downloading validation data')
            urllib.request.urlretrieve('https://www.dropbox.com/s/hu9oc9z5l4wqnuc/doctors_dilemma_valid.csv?dl=1',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'doctors_dilemma_valid.csv'))

            print('downloading test queries')
            urllib.request.urlretrieve('http://www.intellifind.dk/doctors_dilemma/test_queries.csv',
                                       os.path.join(self.environment['config']['data']['data_dir'],
                                                    'test_queries.csv'))

            print ('Data download complete')

        print(' - loading data')

        fz_str2cui = pickle.load(open(os.path.join(self.environment['config']['data']['data_dir'], 'fz_str2cui.pkl'), 'rb'))
        self.text_num2text_and_abstract = {}
        with ZipFile(os.path.join(self.environment['config']['data']['data_dir'], 'text.zip'), 'r') as ziparchive:
            for count, filename in enumerate(ziparchive.namelist()[1:]):  # don't include the containing folder as file
                with ziparchive.open(filename, 'r') as f:
                    text = f.read().decode('utf8')
                    idx = text.find('\r\n')
                    title = text[0:idx]
                    filename = filename[(filename.find('/')+1)::]
                    ngrams = sum([get_n_grams(nltk.word_tokenize(title.lower()),n) for n in range(1,5)], [])
                    candidate_diseases = [n for n in ngrams if n in fz_str2cui]
                    candidate_disease_cuis = list(set([fz_str2cui[n] for n in candidate_diseases]))
                    self.text_num2text_and_abstract[filename] = {'title':title, 'text': text, 'class_':candidate_disease_cuis}

        with ZipFile(os.path.join(self.environment['config']['data']['data_dir'], 'abstracts.zip'), 'r') as ziparchive:
            for count, filename in enumerate(ziparchive.namelist()[1:]):  # don't include the containing folder as file
                with ziparchive.open(filename, 'r') as f:
                    text = f.read().decode('utf8')
                    idx = text.find('\r\n')
                    title = text[0:idx]
                    filename = filename[(filename.find('/')+1)::]
                    assert title == self.text_num2text_and_abstract[filename]['title']
                    self.text_num2text_and_abstract[filename]['abstract'] = text
        with open(os.path.join(self.environment['config']['data']['data_dir'], 'doctors_dilemma_valid.csv'), 'r', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            self.valid_samples = [(row[1], row[0]) for row in reader][1::]
            self.environment['num_validation_samples'] = len(self.valid_samples)

        with open(os.path.join(self.environment['config']['data']['data_dir'], 'test_queries.csv'), 'r', encoding='utf-8') as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            self.test_samples = [(row[2].lower(), row[1]) for row in reader][1::]
            self.environment['num_test_samples'] = len(self.test_samples)


"""
Raw data iterator for the danish findzebra dataset
"""
def html2text(html):
    doc = lh.fromstring(html)
    doc = clean_html(doc)
    doc = doc.text_content()
    doc = re.sub('[\s]{2,}', ' ', doc)
    return doc


class DanishCorpusProvider(BaseProvider):
    def __init__(self):
        pass

    def raw_train_samples_gen(self):
        # Load the raw data as a list of tuples
        print(' - loading data')
        with open(os.path.join(self.environment['config']['data']['data_dir'], 'danish_corpus.csv'), 'r', encoding='utf-8') as f:
            reader = csv.DictReader(f)
            for row in reader:
                row['text'] = html2text(row['content'])
                yield row

    def raw_valid_samples_gen(self):
        return []

    def prepare(self):
        if not os.path.exists(self.environment['config']['data']['data_dir']):
            import urllib.request
            print('creating data directory: %s' % self.environment['config']['data']['data_dir'])
            os.makedirs(self.environment['config']['data']['data_dir'])
            print('downloading training data (~18 MB)')
            urllib.request.urlretrieve('http://www.bioware.dk/danish_corpus/danish_corpus.csv',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'danish_corpus.csv'))


            urllib.request.urlretrieve('http://www.bioware.dk/danish_corpus/cui2danish_names.pkl',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'cui2danish_names.pkl'))



            urllib.request.urlretrieve('http://www.bioware.dk/danish_corpus/cui2prefered_danish_name.pkl',
                                       os.path.join(self.environment['config']['data']['data_dir'], 'cui2prefered_danish_name.pkl'))



    def load_data(self):
        self.environment['num_validation_samples'] = 0


if __name__ == '__main__':
    from flow_framework import *
    flow = [ConfigLoaderFlowComponent('fz search example/bording_config.ini'),
            BordingProvider()]
    env = FlowController(flow).execute()
    for (cui, text) in env['raw_test_samples_gen']():
        print ('%s: %i' % (cui, len(text)))