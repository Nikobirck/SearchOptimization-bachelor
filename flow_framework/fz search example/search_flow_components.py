from flow_framework import *
from copy import deepcopy

class BordingJoinSummaryAndTextFlowComponent(AbstractFlowComponent):
    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                yield sample
                summary_sample = deepcopy(sample)
                summary_sample['text'] = summary_sample['summary']
                yield summary_sample
        return new_generator

    def execute(self, environment={}):
        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        return environment

class CreateCui2docMappingFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def execute(self, environment={}):
        class2samples = {}
        for i, sample in enumerate(environment['raw_train_samples_gen']()):
            if i < 100:
                if not isinstance(sample['text'], list):
                    print("WARNING!!! THE INPUT TO THE COUNT COMPONENT IS ASSUMED TO BE A LIST OF WORDS. "
                          "Input seems to be a list of chars?")
            class_name = sample['class_']
            text = sample['text']
            class2samples.setdefault(class_name, [])
            class2samples[class_name].append(text)
        environment['class2samples'] = class2samples
        return environment

class LoadClass2IdFlowComponent(AbstractFlowComponent):
    def __init__(self, cache_name):
        super(self.__class__, self).__init__()
        self.cache_name=cache_name

    def execute(self, environment={}):
        cache_dir = os.path.join(environment['config']['cache']['cache_dir'], self.cache_name)
        class2id_file = os.path.join(cache_dir, 'class2id.pkl')
        if os.path.isfile(class2id_file):
            class2id = pickle.load(open(class2id_file, 'rb'))
        else:
            class2id = {}
            counter = 0
            for i, sample in enumerate(environment['raw_train_samples_gen']()):
                class_name = sample['class_']
                if class_name not in class2id:
                    class2id[class_name] = counter
                    counter+=1
            pickle.dump(class2id, open(class2id_file, 'wb'))
        environment['class2id'] = class2id
        return environment

