import re, nltk
from flow_framework import *
from metrics import top_k_accuracy
import keras
from proposal_flow_components import *
from proposal_generators import *

if __name__ == '__main__':
    sentences = ["cough, sore throat, fever",
                 "ringing in the ears",
                 "pain in the ear",
                 "high heart rate, palpitations",
                 "sore jaw",
                 "stomachache, vomiting",
                 "fever and stomach pain"]

    metrics = ['accuracy',top_k_accuracy(5),top_k_accuracy(10),top_k_accuracy(20)]
    loss = 'sparse_categorical_crossentropy'
    optimizer = keras.optimizers.Adam(lr=0.001, clipnorm=6.)


    # model_name = 'configs.english.eye_hash_emb'
    models = []
    for token_type in ['unigram', 'n_gram']:
        for num_words_or_buckets in [1000, 5000, 10000, 15000, 20000]:
            models.append(('configs.english.hash_emb', -1, token_type, num_words_or_buckets))
            models.append(('configs.english.std_emb', num_words_or_buckets, token_type, -1))
            models.append(('configs.english.rnn_emb', num_words_or_buckets, token_type, -1))
            models.append(('configs.english.rnn_hash', -1, token_type, num_words_or_buckets))


    for model_name, max_words, cache_name, word_or_bucket_count in models:
        provider = FlowController([ConfigLoaderFlowComponent('config.ini'), ExtendedFindzebraProvider()])

        config_spec = '%s_%i_%s_%i' % (model_name, abs(max_words), cache_name, abs(word_or_bucket_count))
        final_log_filename = config_spec + '.txt'
        if os.path.isfile(os.path.join('results', final_log_filename)):
            continue

        # weights_name = model_name
        checkpoint_path = 'checkpoints/{}'.format(config_spec)

        num_buckets_flow = []
        if word_or_bucket_count > 0:
            num_buckets_flow = [SetConfigValFlowComponent(['model', 'num_buckets'], word_or_bucket_count)]
        num_buckets_flow_controller = FlowController(num_buckets_flow)


        max_words_flow = []
        if max_words > 0:
            max_words_flow = [SetConfigValFlowComponent(['tokens', 'max_tokens'],max_words), MaxWordsFlowComponent()]
        max_words_flowcontroler = FlowController(max_words_flow)

        ngram_flow = []
        if not (cache_name == 'unigram'):
            ngram_flow = [NGramFlowComponent('bigram.pkl', cache_name), NGramFlowComponent('trigram.pkl', cache_name)]
        ngram_flowcontroler = FlowController(ngram_flow)


        flow = [
            provider,
            RemovePunktFlowComponent('’–,|—“.-_”/"*\'=:'),
            LowercaseFlowComponent(),
            WordTokenizeFlowComponent(),
            ngram_flowcontroler,
            CountWordsFlowComponent(cache_name),
            LoadToken2IdFlowComponent(cache_name),
            PruneInfrequentWords(min_token_count=5),
            max_words_flowcontroler,
            NgramVocabAdjustment('token2id'), # force the ngram detector to only use ngrams in token2id
            LoadCui2NamesFlowComponent(), # add a cui2names and cui2prefered_name dictionaries into the environment
            LoadSymptomDicts(cache_name), # target_cui2doc_id_symptom_name_pair, id2doc (id -> document), token2count
            LoadClass2IdFlowComponent(cache_name),
            SplitTrainTestFlowComponent(),
            Leave1OutGenerator(cache_name),
            CreateModelFlowComponent(model_name),
            LoadBestWeightsFlowComponent(checkpoint_path),
            LogModelTestPerformanceFlowComponent(max_n=20, log_dir='results', log_file=final_log_filename),
            CompileModelFlowComponent(loss=loss, metrics=metrics, optimizer=optimizer),
            AddModelSaveCallbackFlowComponent(checkpoint_path),
            AddPrintTopWordsCallbackFlowComponent(),
            AddShowPredictionsCallbackFlowComponent(sentences),
            AddLogProgressCallbackFlowComponent('logs', model_name + '.csv'),
            AddEarlyStoppingCallbackFlowComponent(metric='val_loss', patience=10),
            AddCheckpointsCleanCallbackFlowComponent(checkpoint_path),
            # PackageModelFiles(model_dir, model_name),
            TrainModelFlowComponent(),
            LoadBestWeightsFlowComponent(checkpoint_path),
            LogModelTestPerformanceFlowComponent(max_n=20, log_dir='results', log_file=final_log_filename),
            # CopyBestWeightsFlowComponent(checkpoint_path, model_dir, 'best.hdf5')
            ]

        environment = FlowController(flow_components=flow).execute({})
        environment = None


