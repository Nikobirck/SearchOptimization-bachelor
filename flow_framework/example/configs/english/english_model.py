from keras.models import Model
from keras.layers import Dense, Activation, TimeDistributed, Dropout
from keras.layers import BatchNormalization, Input, merge
from keras.regularizers import l2
from layers import Attention, MultiHashingLayerDropout, ReduceSum, TemporalDropOut
import pickle
import numpy as np
import keras.backend as K
from abstract_config import AbstractConfig

class Config(AbstractConfig):
    reg_factor   = 0.000000  # regularization factor lambda
    base_size = 256
    softmax_size = 512
    aggregation_function = 'sum'
    # aggregation_function = 'concatenate'
    num_buckets = 5000
    embedding_size = 250
    num_hashes = 5
    p_init_std = 0.0005


    def create_model(self, environment):
        self.num_classes = max(environment['class2id'].values())+1
        num_words = max(environment['token2id'].values())+1 # num_words

        self.reg_factor_p = 1.0/(num_words*self.num_hashes*self.p_init_std)

        W = np.random.normal(0, 0.1, (self.num_buckets, self.embedding_size))
        snippet_size = int(environment['config']['sampling']['snippet_size'])
        input_chars = Input([None],dtype='int32', name='input_chars')

        p_init = np.random.normal(0, self.p_init_std, (num_words, self.num_hashes))

        m = MultiHashingLayerDropout(W=W, word_count_including_zero_token=num_words, p_init=p_init, aggregation_mode=self.aggregation_function,
                                     W_trainable=True, p_trainable=True, append_weight=True, mask_zero=True, reg_factor=self.reg_factor_p)
        x = m(input_chars)
        x = Dropout(0.5)(x)
        # x = TemporalDropOut(0.1)(x)
        x = BatchNormalization()(x)

        x = TimeDistributed(Dense(self.base_size*2, activation='tanh'))(x)
        x = BatchNormalization()(x)

        # layer 2 RNN
        # x = TemporalDropOut(0.3)(x)

        x = Dropout(0.5)(x)
        x = ReduceSum()([x, input_chars])
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)

        x = Dense(self.softmax_size,activation='relu')(x)
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)

        x = Dense(self.softmax_size,activation='relu')(x)
        # x = merge([x, res], 'sum')
        x = BatchNormalization()(x)
        x = Dropout(0.5)(x)

        x = Dense(self.num_classes, W_regularizer=l2(self.reg_factor))(x)

        x = BatchNormalization()(x)
        # x = Dropout(0.5)(x)

        x = Activation('softmax')(x)



        environment['model'] = Model(input=input_chars, output=x)
        environment['multi_hashing_layer'] = m

        return environment
