from keras.models import Model
from keras.layers import Dense, Activation, TimeDistributed, Dropout, Embedding
from keras.layers import BatchNormalization, Input, merge
from keras.regularizers import l2
import pickle
import numpy as np
import keras.backend as K
from flow_framework import AbstractConfig
from layers import MultiHashingLayerDropout, ReduceSum

class Config(AbstractConfig):
    base_size = 2000
    aggregation_function = 'sum'
    embedding_size = 500
    p_init_std = 0.0005


    def create_model(self, environment):
        self.num_classes = max(environment['class2id'].values())+1
        num_words = max(environment['token2id'].values())+1 # num_words
        self.num_buckets = int(environment['config']['model']['num_buckets'])
        self.num_hashes = int(environment['config']['model']['num_hashes'])

        input_words = Input([None], dtype='int32', name='input_words')

        self.reg_factor_p = 0.01 / (num_words * self.num_hashes * self.p_init_std)
        p_init = np.random.normal(0, self.p_init_std, (num_words, self.num_hashes))
        W = np.eye(self.num_buckets, self.num_buckets, dtype='float32')
        embedding = MultiHashingLayerDropout(W=W, word_count_including_zero_token=num_words, p_init=p_init, aggregation_mode=self.aggregation_function,
                                     W_trainable=False, p_trainable=True, append_weight=True, mask_zero=True, reg_factor=self.reg_factor_p)

        # embedding = Embedding(num_words, num_words, weights=[np.eye(num_words, num_words, dtype='float32')])
        # embedding.trainable_weights = []

        x = embedding(input_words)
        x = ReduceSum()([x, input_words])

        # x = Dropout(p=0.5)(x)
        x = BatchNormalization()(x)
        x = Dense(self.base_size, activation='relu')(x)

        # x = Dropout(p=0.5)(x)
        x = BatchNormalization()(x)
        x = Dense(self.base_size, activation='relu')(x)

        # x = Dropout(p=0.5)(x)
        x = BatchNormalization()(x)
        x = Dense(self.base_size, activation='relu')(x)
        x = BatchNormalization()(x)

        x = Dense(self.num_classes)(x)
        x = BatchNormalization()(x)
        x = Activation('softmax')(x)

        environment['model'] = Model(input=input_words, output=x)
        environment['multi_hashing_layer'] = embedding

        return environment
