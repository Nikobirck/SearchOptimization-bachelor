import urllib.request
from flow_framework import AbstractFlowComponent
import pickle
import csv, os, nltk
import numpy as np


def encode(text, environment):
    for t in ([t[0] for t in environment['transformations']]):
        text = t(text, environment)
    return text

"""
LoadSymptomDicts is a specialized flowcomponent. The environment output consists of four dictionaries:
1) target_cui2doc_id_symptom_name_pair: keys=symptom target cuis, values=(doc_id, name_tok, start_indices).
The name_tok is the tokenized name of the target symptom occurring in the text e.g. ['back', 'pain']. The start_indices is a
 list of start indices of the name in the tokenized text
2) id2doc: keys= document ids, values=tokenized text documents
3) class2id: target cui -> id
The three dictionaries will be saved in data_dir
"""
class LoadSymptomDicts(AbstractFlowComponent):
    def __init__(self, cache_name):
        super(self.__class__, self).__init__()
        self.cache_name=cache_name

    def execute(self, environment={}):
        doc_id = 0
        id2doc = {}
        cui2names = environment['cui2names']
        cui2prefered_name = environment['cui2prefered_name']
        cui2names = dict([(cui, names) for cui, names in cui2names.items() if cui in cui2prefered_name])


        target_symptom_name2cui = {}
        max_ngram = 0
        for cui in cui2names:
            names = cui2names[cui]
            for name in names:
                name = encode(name, environment)
                max_ngram = max(len(name), max_ngram)
                name = ' '.join(name)
                target_symptom_name2cui[name] = cui
        cache_dir = os.path.join(environment['config']['cache']['cache_dir'], self.cache_name)

        target_cui2doc_id_symptom_name_pair = {}
        if (not os.path.isfile(os.path.join(cache_dir, 'target_cui2doc_id_symptom_name_pair.pkl'))) \
                or (not os.path.isfile(os.path.join(cache_dir, 'id2doc.pkl'))) \
                    or (not os.path.isfile(os.path.join(cache_dir, 'cui2id.pkl')))\
                    or (not os.path.isfile(os.path.join(cache_dir, 'token2count.pkl'))):
            if not os.path.exists(cache_dir):
                os.makedirs(cache_dir)
            cui2id = dict([(cui, i) for i, cui in enumerate(cui2names)])
            pickle.dump(cui2id, open(os.path.join(cache_dir, 'cui2id.pkl'), 'wb'))

            sample_counter = 0
            for counter, sample in enumerate(environment['raw_train_samples_gen']()):
                token_doc = sample['text']
                n_grams = sum([get_n_grams(token_doc, i) for i in range(1, max_ngram)],[])
                targets = list(set([n_gram for n_gram in n_grams if n_gram in target_symptom_name2cui]))

                if counter % 100 == 0:
                    print ('Extracted targets from %i documents. Found %i samples' % (counter, sample_counter))
                if len(targets) == 0:
                    continue
                id2doc[doc_id] = token_doc
                num_words_in_doc = len(token_doc)
                for target_name in targets:
                    target_cui = target_symptom_name2cui[target_name]

                    # find start_indices and end indices for target
                    target_name_tok = nltk.word_tokenize(target_name)
                    candidate_start_indices = [i for i, t in enumerate(token_doc)
                                               if t == target_name_tok[0] and (i+len(target_name_tok)) <= num_words_in_doc]

                    candidate_start_indices = [i for i in candidate_start_indices
                                               if token_doc[i:(i+len(target_name_tok))] == target_name_tok]
                    sample_counter += len(candidate_start_indices)
                    target_cui2doc_id_symptom_name_pair.setdefault(target_cui, [])
                    target_cui2doc_id_symptom_name_pair[target_cui].append((doc_id, target_name_tok, candidate_start_indices))
                doc_id += 1
            pickle.dump(target_cui2doc_id_symptom_name_pair, open(os.path.join(cache_dir, 'target_cui2doc_id_symptom_name_pair.pkl'), 'wb'))
            pickle.dump(id2doc, open(os.path.join(cache_dir, 'id2doc.pkl'), 'wb'))
        else:
            target_cui2doc_id_symptom_name_pair = pickle.load(open(os.path.join(cache_dir, 'target_cui2doc_id_symptom_name_pair.pkl'), 'rb'))
            id2doc = pickle.load(open(os.path.join(cache_dir, 'id2doc.pkl'), 'rb'))

        environment['target_cui2doc_id_symptom_name_pair'] = target_cui2doc_id_symptom_name_pair
        environment['id2doc'] = id2doc
        return environment


def get_n_grams(words, n):
    n_grams = []
    for i in range(0, len(words) - n + 1):
        n_grams.append(' '.join(words[i:(i + n)]))
    return n_grams




class LoadClass2IdFlowComponent(AbstractFlowComponent):
    def __init__(self, cache_name):
        super(self.__class__, self).__init__()
        self.cache_name = cache_name

    def execute(self, environment={}):
        data_dir = os.path.join(environment['config']['cache']['cache_dir'], self.cache_name)
        cui2id = pickle.load(open(os.path.join(data_dir, 'cui2id.pkl'), 'rb'))

        environment['class2id'] = cui2id
        return environment


class LoadCustomCui2PreferedNamesFlowComponent(AbstractFlowComponent):
    def __init__(self, target_names_file):
        super(self.__class__, self).__init__()
        self.target_names_file = target_names_file

    def execute(self, environment={}):
        target_names = open(self.target_names_file).read().split('\n')
        cui2names = environment['cui2names']
        cui2prefered_name = environment['cui2prefered_name']
        name2cui = {}
        for cui, names in cui2names.items():
            for name in names:
                name2cui[name] = cui

        for name in target_names:
            if name not in name2cui:

                print(name + ' not in name2cui')
                continue
            cui = name2cui[name]
            if cui in cui2prefered_name:
                continue
            else:
                cui2prefered_name[cui] = name
        environment['cui2prefered_name'] = cui2prefered_name
        return environment



class LoadCui2NamesFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def execute(self, environment={}):
        cui2names_file = os.path.join(environment['config']['data']['data_dir'], 'cui2english_names.pkl')
        cui2prefered_names_file = os.path.join(environment['config']['data']['data_dir'], 'cui2prefered_english_name.pkl')
        if not os.path.isfile(cui2names_file):
            print('downloading cui2english_names.pkl')
            urllib.request.urlretrieve('http://www.intellifind.dk/ic/cui2english_names.pkl', cui2names_file)
        if not os.path.isfile(cui2prefered_names_file):
            print('downloading cui2prefered_english_name.pkl')
            urllib.request.urlretrieve('http://www.intellifind.dk/ic/cui2prefered_english_name.pkl',cui2prefered_names_file)

        cui2names = pickle.load(open(cui2names_file, 'rb'))
        cui2prefered_names = pickle.load(open(cui2prefered_names_file, 'rb'))

        cui2names = dict([(cui, names) for (cui, names) in cui2names.items() if cui in cui2prefered_names])

        environment['cui2names'] = cui2names
        environment['cui2prefered_name'] = cui2prefered_names
        return environment






class SplitTrainTestFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def execute(self, environment={}):
        self.valid_fraction = environment['config']['data']['valid_fraction']
        if environment['num_validation_samples'] > 0 \
                and int(environment['config']['data']['use_separate_validation_set']) == 1 :
            environment['target_cui2doc_id_symptom_name_pair_train'] = environment['target_cui2doc_id_symptom_name_pair']
            return environment
        np.random.seed(0)
        target_cui2doc_id_symptom_name_pair = environment['target_cui2doc_id_symptom_name_pair']
        target_cui2doc_id_symptom_name_pair_train = {}
        target_cui2doc_id_symptom_name_pair_valid = {}
        for cui, triplets in target_cui2doc_id_symptom_name_pair.items():

            for triplet in triplets:
                target = target_cui2doc_id_symptom_name_pair_valid
                if np.random.random() > self.valid_fraction:
                    target = target_cui2doc_id_symptom_name_pair_train
                target.setdefault(cui, [])
                target[cui].append(triplet)
        environment['target_cui2doc_id_symptom_name_pair_train'] = target_cui2doc_id_symptom_name_pair_train
        environment['target_cui2doc_id_symptom_name_pair_valid'] = target_cui2doc_id_symptom_name_pair_valid
        environment['num_validation_samples'] = 1000
        return environment


if __name__ == '__main__':
    from raw_data_providers import ExtendedFindzebraProvider, DanishCorpusProvider
    from flow_components import ConfigLoaderFlowComponent, FlowController, RemovePunktFlowComponent, WordTokenizeFlowComponent
    flow = [ConfigLoaderFlowComponent('config_dk.ini'),
            # ExtendedFindzebraProvider(),
            DanishCorpusProvider(),
            WordTokenizeFlowComponent(),
            LoadCui2NamesFlowComponent(),  # add a cui2names and cui2prefered_name dictionaries to the environment
            LoadSymptomDicts(),
            SplitTrainTestFlowComponent()
            ]
    env = FlowController(flow).execute()