from gensim.models.phrases import Phrases
import shutil
import abc
import configparser
import os
import pickle
import operator
from stemming.porter2 import stem
import numpy as np
from importlib import import_module
import random
from gensim.models import Word2Vec

class AbstractFlowComponent(object):
    @abc.abstractmethod
    def execute(self, environment={}):
        pass

"""
An AbstractTransformerFlowComponent is a flow component that modifies the iterators in some way. E.g. a stemming component
expects a tokenized stream and modifies the data stream by performing stemming.
"""
class AbstractTransformerFlowComponent(AbstractFlowComponent):
    def __init__(self):
        pass

    @abc.abstractmethod
    def transform(self, text, env=None):
        pass

    """
    get_dependencies return the environment variables that are used by the transformer
    """
    def get_dependencies(self):
        return []


class CompileModelFlowComponent(AbstractFlowComponent):
    def __init__(self, loss, metrics,optimizer):
        super(self.__class__, self).__init__()
        self.loss = loss
        self.metrics = metrics
        self.optimizer = optimizer

    def execute(self, environment={}):
        # Compile the model so it is ready for training
        print ('Compiling model...')
        environment['model'].compile(optimizer=self.optimizer,
                             loss=self.loss,
                             metrics=self.metrics)
        return environment


class ConfigLoaderFlowComponent(AbstractFlowComponent):
    # config file can be the name of a config file, or a dict
    def __init__(self, config_file):
        super(self.__class__, self).__init__()
        self.config_file = config_file


    def execute(self, environment={}):
        if 'config' in environment:
            config = environment['config']
        else:
            config = {}
        if not os.path.isfile(self.config_file):
            msg = 'Config file not found!: %s' % self.config_file
            print (msg)
            raise FileNotFoundError(msg)
        self.config = configparser.ConfigParser()
        if isinstance(self.config_file, dict):
            for section in self.config_file:
                for option in self.config_file[section]:
                    self.config.set(section, option, self.config_file[section][option])
        else:
            self.config.read(self.config_file)
        for section in self.config.sections():
            if not section in config:
                config[section] = {}
            for option in self.config.options(section):
                config[section][option] = self.config.get(section, option)
        environment['config'] = config
        return environment



class CreateModelFlowComponent(AbstractFlowComponent):
    def __init__(self, config_module):
        super(self.__class__, self).__init__()
        self.config_module = config_module

    def execute(self, environment={}):
        print ('Creating model ' + self.config_module)
        environment['model_name'] = self.config_module
        config = import_module('{}'.format(self.config_module)).Config()
        environment = config.create_model(environment)
        print("Number of parameters in model: %i" % environment['model'].count_params())
        return environment



class LoadBestWeightsFlowComponent(AbstractFlowComponent):
    def __init__(self, weights_dir):
        super(self.__class__, self).__init__()
        self.weights_dir = weights_dir

    def execute(self, environment={}):
        if os.path.exists(self.weights_dir):
            checkpoint_files = os.listdir(self.weights_dir)
            if checkpoint_files:
                latest_checkpoint = sorted(os.listdir(self.weights_dir))[0]
                print('Resuming training from latest checkpoint: {}'.format(latest_checkpoint))
                environment['model'].load_weights(self.weights_dir + '/' + latest_checkpoint)
        return environment


class LogModelTestPerformanceFlowComponent(AbstractFlowComponent):
    def __init__(self, max_n, log_dir, log_file):
        super(self.__class__, self).__init__()
        self.max_n = max_n
        self.log_dir = log_dir
        self.log_file = log_file

    def execute(self, environment={}):
        encoded_text, classes = next(environment['encoded_test_samples_gen']())
        if not os.path.isdir(self.log_dir):
            os.makedirs(self.log_dir)
        predicted_classes = environment['model'].predict(encoded_text)
        predicted_classes = np.argsort(-predicted_classes)
        classes = np.repeat(classes,predicted_classes.shape[1], axis=1)
        res = np.equal(classes, predicted_classes).astype('int32')
        recall_at_n = ['%1.4f' % np.mean(np.sum(res[:,0:n], axis=1)) for n in range(1, self.max_n+1)]
        open(os.path.join(self.log_dir, self.log_file), 'w').write('\t'.join(recall_at_n))
        return environment

"""
Transform the tokenized raw streams to n-grams
"""
class NGramFlowComponent(AbstractTransformerFlowComponent):
    def __init__(self, ngram_outputfile_name, cache_name, phrases_args={}):
        super(self.__class__, self).__init__()
        self.ngram_outputfile_name = ngram_outputfile_name
        self.cache_name = cache_name
        self.phrases_args = phrases_args

    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                sample['text'] = self.transform(sample['text'])
                yield sample
        return new_generator


    def transform(self, text, env=None):
        if not isinstance(text, list):
            print ("WARNING!!! THE INPUT TO THE STEMMING COMPONENT IS ASSUMED TO BE A LIST OF WORDS. "
                   "Input seems to be a list of chars")
        return list(self.bigram[text])


    def execute(self, environment={}):
        np.random.seed(2)
        ngram_dir = os.path.join(environment['config']['cache']['cache_dir'],self.cache_name, 'ngrams')
        if not os.path.isdir(ngram_dir):
            os.makedirs(ngram_dir)
        print ('Using n-grams')
        import logging
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

        ngram_file_full_path = os.path.join(ngram_dir, self.ngram_outputfile_name)
        if os.path.isfile(ngram_file_full_path):
            self.bigram = Phrases.load(ngram_file_full_path)
        else:
            texts = [row['text'] for i, row in enumerate(environment['raw_train_samples_gen']())]
            self.bigram = Phrases(texts,**self.phrases_args)
            self.bigram.save(ngram_file_full_path)


        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        environment['raw_valid_samples_gen'] = self.generator_generator(environment['raw_valid_samples_gen'])
        environment['raw_test_samples_gen'] = self.generator_generator(environment['raw_test_samples_gen'])
        if 'transformations' not in environment:
            environment['transformations'] = []
        environment['transformations'].append((self.transform, [], self.__class__.__name__))
        environment.setdefault('ngram_dicts',[])
        environment['ngram_dicts'].append(self.bigram.vocab)
        return environment

"""
NgramVocabAdjustment removes the ngrams from gensim ngram detector, that are not in the dict environment[key]
"""
class NgramVocabAdjustment(AbstractFlowComponent):
    def __init__(self, key):
        super(self.__class__, self).__init__()
        self.key = key

    def execute(self, environment={}):
        target_dict = environment[self.key]
        if 'ngram_dicts' not in environment:
            return environment
        for ngram_dict in environment['ngram_dicts']:
            ngrams = list(ngram_dict.keys())
            for w in ngrams:
                if w.decode("utf-8") not in target_dict:
                    ngram_dict.__delitem__(w)
        return environment


class CopyFilesFlowComponent(AbstractFlowComponent):
    def __init__(self, source_files, dest_files):
        super(self.__class__, self).__init__()
        if not isinstance(source_files, list):
            self.source_files = [source_files]
        if not isinstance(dest_files, list):
            self.dest_files = [dest_files]

    def execute(self, environment={}):
        for src, dest in zip(self.source_files, self.dest_files):
            shutil.copy(src, dest)
        return environment


class CopyBestWeightsFlowComponent(AbstractFlowComponent):
    def __init__(self, weights_dir, out_dir, out_file):
        super(self.__class__, self).__init__()
        self.weights_dir = weights_dir
        self.out_dir = out_dir
        self.out_file = out_file

    def execute(self, environment={}):
        if not os.path.exists(self.out_dir):
            os.makedirs(self.out_dir)
        if os.path.exists(self.weights_dir):
            checkpoint_files = os.listdir(self.weights_dir)
            if checkpoint_files:
                best_checkpoint = sorted(os.listdir(self.weights_dir))[0]
                shutil.copy(os.path.join(self.weights_dir, best_checkpoint), os.path.join(self.out_dir,self.out_file))
        return environment

class CountWordsFlowComponent(AbstractFlowComponent):
    def __init__(self, cache_name):
        super(self.__class__, self).__init__()
        self.cache_name = cache_name

    def execute(self, environment={}):
        cache_dir = os.path.join(environment['config']['cache']['cache_dir'], self.cache_name)
        if not os.path.exists(cache_dir):
            os.makedirs(cache_dir)
        counts_file = os.path.join(cache_dir, 'token2count.pkl')
        if os.path.isfile(counts_file):
            environment['token2count'] = pickle.load(open(counts_file, 'rb'))
        else:
            token2count = {}
            for i, sample in enumerate(environment['raw_train_samples_gen']()):
                if i < 100:
                    if not isinstance(sample['text'], list):
                        print("WARNING!!! THE INPUT TO THE COUNT COMPONENT IS ASSUMED TO BE A LIST OF WORDS. "
                              "Input seems to be a list of chars?")
                for t in sample['text']:
                    token2count.setdefault(t,0)
                    token2count[t] += 1
            environment['token2count'] = token2count
            pickle.dump(token2count, open(counts_file, 'wb'))

        return environment


class LoadClass2samplesFlowComponent(AbstractFlowComponent):
    def __init__(self, cache_name):
        super(self.__class__, self).__init__()
        self.cache_name = cache_name

    def execute(self, environment={}):
        file_name = os.path.join(environment['config']['cache']['cache_dir'], self.cache_name, 'class2samples.pkl')

        if os.path.isfile(file_name):
            print ('Loading class2samples...')
            class2samples = pickle.load(open(file_name, mode='rb'))
            print ('Done loading class2samples')
        else:
            print ('Creating class2samples...')
            class2samples = {}
            for i, sample in enumerate(environment['raw_train_samples_gen']()):
                (class_name, text) = sample['class_'], sample['text']
                class2samples.setdefault(class_name,[])
                class2samples[class_name].append(text)
                if i % 1000 == 0:
                    print (i)

            pickle.dump(class2samples, open(file_name, 'wb'))
            print ('Done creating class2samples')

        environment['class2samples'] = class2samples
        return environment



class LoadWeightsFromFileFlowComponent(AbstractFlowComponent):
    def __init__(self, weights_file):
        super(self.__class__, self).__init__()
        self.weights_file = weights_file

    def execute(self, environment={}):
        if not os.path.exists(self.weights_file):
            raise FileNotFoundError('The weights %s do not exist')
        environment['model'].load_weights(self.weights_file)
        return environment

class AddModelToEnsembleFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def execute(self, environment={}):
        if 'ensemble_models' not in environment:
            environment['ensemble_models'] = []
        environment['ensemble_models'].append(environment['model'])
        if 'ensemble_transformations' not in environment:
            environment['ensemble_transformations'] = []

        transformations = ([t[0] for t in environment['transformations']])
        def encode(text):
            for t in transformations:
                text = t(text, environment)
            return text

        environment['ensemble_transformations'].append(encode)
        return environment


class MaxWordsFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()


    def execute(self, environment={}):
        np.random.seed(2)
        max_words = int(environment['config']['tokens']['max_tokens'])
        if max_words < 0:
            return
        token_dict = environment['token2id']

        print ('Using only the most frequent %i words' % max_words)
        token2count = environment['token2count']
        if max_words < len(token_dict):
            sorted_words = sorted(token2count.items(), key=operator.itemgetter(1), reverse=True)
            sorted_words = dict([(s, c) for s, c in sorted_words][0:max_words])
            sorted_words = [w for w, _ in sorted(token_dict.items(), key=lambda x: x[1]) if w in sorted_words]
            environment['token2id'] = dict(zip(sorted_words, range(0, max_words)))
        return environment


"""
A tokenize component expects a text to be a string. Output are the tokenized words.
"""
class WordTokenizeFlowComponent(AbstractTransformerFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                sample['text'] = self.transform(sample['text'])
                yield sample
        return new_generator


    def transform(self, text, env=None):
        import nltk
        if not isinstance(text, str):
            print ("WARNING!!! THE INPUT TO THE tokenize COMPONENT IS ASSUMED TO BE A string."
                   "Input seems to be a list of chars")
        return nltk.word_tokenize(text)


    def execute(self, environment={}):
        # self.all_samples = []
        # for sample in  self.generator_generator(environment['raw_train_samples_gen'])():
        #     self.all_samples.append(sample)
        # def raw_train_samples_gen():
        #     for sample in self.all_samples:
        #         yield sample
        # environment['raw_train_samples_gen'] = raw_train_samples_gen

        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        environment['raw_valid_samples_gen'] = self.generator_generator(environment['raw_valid_samples_gen'])
        environment['raw_test_samples_gen'] = self.generator_generator(environment['raw_test_samples_gen'])
        if 'transformations' not in environment:
            environment['transformations'] = []
        environment['transformations'].append((self.transform, [], self.__class__.__name__))
        return environment

"""
A tokenize component expects a text to be a string. Output are the tokenized words.
"""
class CharTokenizeFlowComponent(AbstractTransformerFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                sample['text'] = self.transform(sample['text'])
                yield sample
        return new_generator


    def transform(self, text, env=None):

        if not isinstance(text, str):
            print ("WARNING!!! THE INPUT TO THE tokenize COMPONENT IS ASSUMED TO BE A string.")
        return [str(c) for c in text]


    def execute(self, environment={}):
        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        environment['raw_valid_samples_gen'] = self.generator_generator(environment['raw_valid_samples_gen'])
        environment['raw_test_samples_gen'] = self.generator_generator(environment['raw_test_samples_gen'])
        if 'transformations' not in environment:
            environment['transformations'] = []
        environment['transformations'].append((self.transform, [], self.__class__.__name__))
        return environment


"""
MergeTrainSamplesGeneratorsFlowComponent merges the train samples generators from several flows. One of the flows is
the main flow, meaning that only the state of that environment is used after the merge (in order to solve conflicts
from e.g. config files (they could for example both have a data_dir entry)
"""
class MergeTrainSamplesGeneratorsFlowComponent(AbstractFlowComponent):
    def __init__(self, main_flow, secondary_flows=[]):
        super(self.__class__, self).__init__()
        self.main_flow = main_flow
        self.secondary_flows = secondary_flows

    def raw_train_samples_gen(self, generators):
        def new_generator():
            for generator in generators:
                for sample in generator():
                    yield sample
        return new_generator

    def execute(self, environment={}):
        train_generators = []
        main_environment = self.main_flow.execute({})
        train_generators.append(main_environment['raw_train_samples_gen'])
        for component in self.secondary_flows:
            environment = component.execute({})
            train_generators.append(environment['raw_train_samples_gen'])
        main_environment['raw_train_samples_gen'] = self.raw_train_samples_gen(train_generators)
        return main_environment

"""
CacheTrainGeneratorGeneratorsFlowComponent makes a cached copy of the train the raw_train_samples_gen. The purpose of this
 is to avoid having to e.g. tokenize more than once.
"""
class CacheTrainGeneratorGeneratorsFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def raw_train_samples_gen(self, generator):
        def new_generator():
            for sample in generator:
                yield sample
        return new_generator

    def execute(self, environment={}):
        all_samples = []
        for s in environment['raw_train_samples_gen']():
            all_samples.append(s)
        environment['raw_train_samples_gen'] = self.raw_train_samples_gen(all_samples)
        return environment

"""
A stemming component expects a text to be a list of tokens. Output are the stemmed words.
"""
class StemmmingFlowComponent(AbstractTransformerFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                sample['text'] = self.transform(sample['text'])
                yield sample
        return new_generator


    def transform(self, text, env=None):
        if not isinstance(text, list):
            print ("WARNING!!! THE INPUT TO THE STEMMING COMPONENT IS ASSUMED TO BE A LIST OF WORDS. "
                   "Input seems to be a list of chars")
        return [stem(w) for w in text]


    def execute(self, environment={}):
        np.random.seed(2)
        token_dict = environment['token2id']
        print ('Using stemming!')
        counter = 0

        word_dict_new = {}
        stem2word = {}
        for w in token_dict:
            s_w = stem(w)
            if s_w not in stem2word:
                stem2word[s_w] = counter
                word_dict_new[w] = counter
                counter += 1
            else:
                word_dict_new[w] = stem2word[s_w]
        print ('Stemming reduced token count from %i to %i' %
               (len(token_dict), len(list(set(word_dict_new.values())))))
        environment['token2id'] = word_dict_new
        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        environment['raw_valid_samples_gen'] = self.generator_generator(environment['raw_valid_samples_gen'])
        environment['raw_test_samples_gen'] = self.generator_generator(environment['raw_test_samples_gen'])
        if 'transformations' not in environment:
            environment['transformations'] = []
        environment['transformations'].append((self.transform, [], self.__class__.__name__))
        return environment


class TrainModelFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def execute(self, environment={}):
        valid_batch_generator = environment['encoded_valid_samples_gen']
        train_batch_generator = environment['encoded_train_samples_gen']

        # Start the training loop
        print('Starting training...')
        num_valid_samples =int(environment['num_validation_samples'])
        num_batches_pr_epoch = int(environment['config']['sampling']['num_batches_pr_epoch'])
        if num_valid_samples>0:
            environment['model'].fit_generator(generator=train_batch_generator(),
               samples_per_epoch=num_batches_pr_epoch * int(environment['config']['sampling']['batch_size']),
               nb_epoch=int(environment['config']['sampling']['max_epochs']),
               validation_data=valid_batch_generator(),
               nb_val_samples=int(environment['num_validation_samples']),
               nb_worker=1,
               callbacks=environment['callbacks'])
        else:
            environment['model'].fit_generator(generator=train_batch_generator(),
               samples_per_epoch=num_batches_pr_epoch * int(environment['config']['sampling']['batch_size']),
               nb_epoch=int(environment['config']['sampling']['max_epochs']),
               callbacks=environment['callbacks'])
        return environment

class SetConfigValFlowComponent(AbstractFlowComponent):
    def __init__(self, key, value):
        super(self.__class__, self).__init__()
        self.key = key
        self.value = value

    def execute(self, environment={}):
        c = environment['config']
        to_update = environment['config']
        for key in self.key[0:-1]:
            c = c[key]
        c[self.key[-1]] = self.value
        return environment


class FlowController(AbstractFlowComponent):
    def __init__(self, flow_components=[]):
        super(self.__class__, self).__init__()
        self.flow_components = flow_components

    def execute(self, environment={}):
        for component in self.flow_components:
            print (component.__class__.__name__)
            environment = component.execute(environment)
            assert environment is not None
        return environment



class LowercaseFlowComponent(AbstractTransformerFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def transform(self, text, env=None):
        if not isinstance(text, str):
            print ("WARNING!!! THE INPUT TO THE LowercaseFlowComponent IS ASSUMED TO BE A STRING. "
                   "Input seems to be a list of words")
        return text.lower()

    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                sample['text'] = self.transform(sample['text'])
                yield sample
        return new_generator

    def execute(self, environment={}):
        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        environment['raw_valid_samples_gen'] = self.generator_generator(environment['raw_valid_samples_gen'])
        environment['raw_test_samples_gen'] = self.generator_generator(environment['raw_test_samples_gen'])
        if 'transformations' not in environment:
            environment['transformations'] = []
        environment['transformations'].append((self.transform, [], self.__class__.__name__))
        return environment

class Word2VecFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()
    def execute(self, environment={}):
        # Train word2vec
        texts = [row['text'] for i, row in enumerate(environment['raw_train_samples_gen']())]
        model = Word2Vec(texts, size=int(environment['config']['model']['embedding_size']))
        weights = model.wv.syn0
        environment['word2vec_weights'] = weights
        vocab = dict([(k, v.index) for k, v in model.wv.vocab.items()])
        token2id = {}
        for k, v in vocab.items():
            token2id[k] = v
        environment['token2id'] = token2id
        return environment

class RemovePunktFlowComponent(AbstractTransformerFlowComponent):
    def __init__(self, replacement_chars):
        super(self.__class__, self).__init__()
        self.replacement_chars = replacement_chars

    def transform(self, text, env=None):
        if not isinstance(text, str):
            print ("WARNING!!! THE INPUT TO THE RemovePunktFlowComponent COMPONENT IS ASSUMED TO BE A STRING.")
            if isinstance(text, list):
                print ("Input seems to be a list of words")
        for c in self.replacement_chars:
            text = text.replace(c, ' ')
        return text

    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                sample['text'] = self.transform(sample['text'])
                yield sample
        return new_generator

    def execute(self, environment={}):
        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        environment['raw_valid_samples_gen'] = self.generator_generator(environment['raw_valid_samples_gen'])
        environment['raw_test_samples_gen'] = self.generator_generator(environment['raw_test_samples_gen'])
        if 'transformations' not in environment:
            environment['transformations'] = []
        environment['transformations'].append((self.transform, [], self.__class__.__name__))
        return environment



class PruneInfrequentWords(AbstractFlowComponent):
    def __init__(self, min_token_count):
        super(self.__class__, self).__init__()
        self.min_token_count = min_token_count

    def execute(self, environment={}):
        token2count = environment['token2count']
        token2id_old = environment['token2id']
        token_order = sorted(token2id_old.items(), key=lambda x: x[1])
        token2id = {}
        id_counter = 0
        pruning_count = 0
        for token,_ in token_order:
            count = token2count[token]
            if count >= self.min_token_count:
                token2id[token] = id_counter
                id_counter += 1
            else:
                pruning_count += 1
                # print ('Pruning %s' % token)
        print('Pruned %i tokens' % pruning_count)
        environment['token2id'] = token2id
        return environment

class LoadToken2IdFlowComponent(AbstractFlowComponent):
    def __init__(self, cache_name):
        super(self.__class__, self).__init__()
        self.cache_name = cache_name

    def execute(self, environment={}):
        cache_dir = os.path.join(environment['config']['cache']['cache_dir'], self.cache_name)
        filename = os.path.join(cache_dir, 'token2id.pkl')
        if (not os.path.isfile(filename)):
            token2id = {}
            id_counter = 0
            for sample_num, row in enumerate(environment['raw_train_samples_gen']()):
                if sample_num > 0 and sample_num % 1000 == 0:
                    print (sample_num)
                text = row['text']
                for word in text:
                    if word not in token2id:
                        token2id[word] = id_counter
                        id_counter += 1
            pickle.dump(token2id, open(filename, 'wb'))
        else:
            token2id = pickle.load(open(filename, 'rb'))
        environment['token2id'] = token2id
        return environment


class PackageModelFiles(AbstractFlowComponent):
    def __init__(self, package_dir):
        super(self.__class__, self).__init__()
        self.package_dir = package_dir

    def execute(self, environment={}):
        if not os.path.isdir(self.package_dir):
            print ('Creating directory: ' + self.package_dir)
            os.makedirs(self.package_dir)
        self.model_name = environment['model_name']
        dependencies = sum([t[1] for t in environment['transformations']], []) + ['config', 'class2id']
        dependencies = list(set(dependencies))
        env = dict([(key, environment[key]) for key in dependencies])
        parts = self.model_name.split('.')
        pickle.dump((env,
                     environment['transformations'],
                     parts[-1]),
                    open(os.path.join(self.package_dir, 'environment.pkl'), 'wb'),protocol=4)

        shutil.copy(os.path.join(self.model_name.replace('.', '/')+'.py'),
                    os.path.join(self.package_dir, parts[-1] + '.py'))
        open(os.path.join(self.package_dir,'__init__.py'), 'a').close()
        return environment

class PrintNumTokensFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def execute(self, environment={}):
        print ('Number of words in model: %i ' % len(environment['token2id']))
        return environment

class PrintNumClassesFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def execute(self, environment={}):
        print ('Number of classes in model: %i ' % len(environment['class2id']))
        return environment

class PrintNumDocsFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def execute(self, environment={}):
        document_counter = 0
        for _ in (environment['raw_train_samples_gen']()):
            document_counter += 1
        print ('Number of documents in corpus: %i ' % document_counter)
        return environment



class TrainSamplesSnippetGeneratorsFlowComponent(AbstractFlowComponent):
    def __init__(self):
        super(self.__class__, self).__init__()

    def create_snippet(self, snippet_size):
        key = np.random.choice(self.keys, 1,p=self.weights)[0]
        texts = self.class2samples[key]
        num_texts = len(texts)
        text_num = np.random.randint(0,num_texts,1)[0]
        text = texts[text_num]
        snippet = self.random_substring(text, snippet_size), key
        return snippet

    def execute(self, environment={}):
        self.class2samples = environment['class2samples']
        self.keys = list(self.class2samples.keys())
        self.weights = np.asarray([len(self.class2samples[key]) for key in self.class2samples],dtype='float32')
        self.weights/= np.sum(self.weights)
        environment['raw_snippet_gen'] = self
        return environment

    def random_substring(self, text, length=100):
        """Pick out a random substring of a certain length.

        If the original string is shorter than the desired length of the
        substring, the whole string is returned.
        """
        string_length = len(text)
        if string_length <= length:
            ret_string = text
        else:
            max_start = string_length - length
            start = random.randint(0, max_start)
            ret_string = text[start:start + length]
        return ret_string