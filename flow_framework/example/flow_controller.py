import re, nltk
from flow_framework import CreateModelFlowComponent, LoadBestWeightsFlowComponent, CompileModelFlowComponent
from flow_framework import TrainModelFlowComponent
from flow_framework import AddEarlyStoppingCallbackFlowComponent, AddLogProgressCallbackFlowComponent, AddPrintTopWordsCallbackFlowComponent, AddModelSaveCallbackFlowComponent, AddShowPredictionsCallbackFlowComponent
from metrics import top_k_accuracy
import keras
from flow_framework import ExtendedFindzebraProvider, DanishCorpusProvider
from flow_framework import ConfigLoaderFlowComponent, FlowController, RemovePunktFlowComponent
from proposal_flow_components import SplitTrainTestFlowComponent, LoadCui2NamesFlowComponent, LoadToken2IdFlowComponent, LoadSymptomDicts,LoadClass2IdFlowComponent
from proposal_generators import Leave1OutGenerator

if __name__ == '__main__':
    lang = 'danish'
    assert(lang == 'english' or lang == 'danish')
    if lang == 'danish':
        # model_name = 'danish_model'
        model_name = 'configs.danish.danish_model_large'
        provider = FlowController([ConfigLoaderFlowComponent('config_dk.ini'),DanishCorpusProvider()])
        sentences = ["hosten, ondt i halsen, feber",
                     "ringen for ørene",
                     "ondt i øret",
                     "høj puls, hjertebanken",
                     "ondt i kæben",
                     "ondt i maven, opkast",
                     "blod i afføringen"
                     "feber og mavepine"]
    else:
        model_name = 'configs.english.english_model'
        provider = FlowController([ConfigLoaderFlowComponent('config.ini'), ExtendedFindzebraProvider()])
        sentences = ["cough, sore throat, fever",
                     "ringing in the ears",
                     "pain in the ear",
                     "high heart rate, palpitations",
                     "sore jaw",
                     "stomachache, vomiting",
                     "fever and stomach pain"]
    model_dir = 'model_' + lang
    # model_name = 'simple_sum4'


    # model_name = 'bording_simple_pure_char'
    weights_name = model_name

    checkpoint_path = 'checkpoints/{}'.format(weights_name)

    from flow_framework import ConfigLoaderFlowComponent, FlowController, \
        RemovePunktFlowComponent, LowercaseFlowComponent, WordTokenizeFlowComponent, \
        StemmmingFlowComponent, PackageModelFiles, CopyBestWeightsFlowComponent
    # from prediction import Predictor

    # p = Predictor('test_model')
    # p.load_model('test_model/best.hdf5')
    # # cui2prefered_name = environment['cui2prefered_name']
    # res = p.predict('ondt i maven')
    # # res = [(cui2prefered_name[cui], p, cui) for cui,p in res]
    # print (res)




    create_model = CreateModelFlowComponent(model_name)
    load_best_weights = LoadBestWeightsFlowComponent(checkpoint_path)
    add_model_save_callback = AddModelSaveCallbackFlowComponent(checkpoint_path)
    add_display_top_words = AddPrintTopWordsCallbackFlowComponent()


    metrics = ['accuracy',top_k_accuracy(5),top_k_accuracy(10),top_k_accuracy(20)]
    loss = 'sparse_categorical_crossentropy'
    optimizer = keras.optimizers.Adam(lr=0.001, clipnorm=6.)

    compile_model = CompileModelFlowComponent(loss=loss, metrics=metrics,optimizer=optimizer)

    train_model = TrainModelFlowComponent()


    replacement_chars = '’–,|—“.-_”/"*\'=:'
    def transform_string(s):

        s = s.lower()
        s = re.sub('[' + replacement_chars + ']', ' ', s)
        s = nltk.word_tokenize(s)
        return s

    # flow = [
    #     provider,
    #     RemovePunktFlowComponent(replacement_chars),
    #     LowercaseFlowComponent(),
    #     WordTokenizeFlowComponent(),
    #     LoadCui2NamesFlowComponent(), # add a cui2names and cui2prefered_name dictionaries to the environment
    #     LoadSymptomDicts(), # target_cui2doc_id_symptom_name_pair, id2doc (id -> document), token2count
    #     LoadClass2IdFlowComponent(),
    #     LoadToken2IdFlowComponent(2), # add token2id dictionary to environment. Loaded from token2id.pkl (if it exists).
    #     Leave1OutGenerator(),
    #     PackageModelFiles('test_model', model_name)
    #     ]
    #
    # environment = FlowController(flow_components=flow).execute()
    #
    # p = Predictor('test_model')
    # p.load_model('test_model/best.hdf5')
    # cui2prefered_name = environment['cui2prefered_name']
    # res = p.predict('ondt i maven')
    # res_all = [(cui2prefered_name[cui], p, cui) for cui,p in res]
    # print (res_all)

    # transforms = ([t[0] for t in environment['transformations']])
    # dependencies = sum([t[1] for t in environment['transformations']],[])
    # env = dict([(key, environment[key]) for key in dependencies])
    #
    # def f(text):
    #     retval = [text]
    #     for t in transforms:
    #         text = t(text, env)
    #         retval.append(text)
    #     return retval
    # val = f('the.  patient Was suffering from influenza')





    flow = [
        provider,
        RemovePunktFlowComponent(replacement_chars),
        LowercaseFlowComponent(),
        WordTokenizeFlowComponent(),
        LoadCui2NamesFlowComponent(), # add a cui2names and cui2prefered_name dictionaries to the environment
        LoadSymptomDicts(), # target_cui2doc_id_symptom_name_pair, id2doc (id -> document), token2count
        LoadClass2IdFlowComponent(),
        LoadToken2IdFlowComponent(min_token_count=2), # add token2id dictionary to environment. Loaded from token2id.pkl (if it exists).
        SplitTrainTestFlowComponent(valid_fraction=0.1),
        Leave1OutGenerator(),
        create_model,
        load_best_weights,
        compile_model,
        add_model_save_callback,
        add_display_top_words,
        AddShowPredictionsCallbackFlowComponent(sentences, transform_string),
        AddLogProgressCallbackFlowComponent('logs_'+lang, 'out.csv'),
        AddEarlyStoppingCallbackFlowComponent(metric='val_loss', patience=3),
        PackageModelFiles(model_dir, model_name),
        train_model,
        CopyBestWeightsFlowComponent(checkpoint_path, model_dir, 'best.hdf5')
        ]

    environment = FlowController(flow_components=flow).execute()


