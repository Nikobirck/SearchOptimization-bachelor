from keras.models import Model
from keras.layers import Dense, Activation, TimeDistributed, Dropout, Embedding
from keras.layers import BatchNormalization, Input, merge
from keras.regularizers import l2
import pickle
import numpy as np
import keras.backend as K
from flow_framework import AbstractConfig
from layers import MultiHashingLayerDropout, ReduceSum

class Config(AbstractConfig):
    base_size = 2000


    def create_model(self, environment):
        self.num_classes = max(environment['class2id'].values())+1
        num_words = max(environment['token2id'].values())+1 # num_words

        input_words = Input([None], dtype='int32', name='input_words')

        embedding = Embedding(num_words, num_words, weights=[np.eye(num_words, num_words, dtype='float32')])
        embedding.trainable_weights = []

        x = embedding(input_words)
        x = ReduceSum()([x, input_words])

        # x = Dropout(p=0.5)(x)
        x = BatchNormalization()(x)
        x = Dense(self.base_size, activation='relu')(x)

        # x = Dropout(p=0.5)(x)
        x = BatchNormalization()(x)
        x = Dense(self.base_size, activation='relu')(x)

        # x = Dropout(p=0.5)(x)
        x = BatchNormalization()(x)
        x = Dense(self.base_size, activation='relu')(x)
        x = BatchNormalization()(x)

        x = Dense(self.num_classes)(x)
        x = BatchNormalization()(x)
        x = Activation('softmax')(x)

        environment['model'] = Model(input=input_words, output=x)

        return environment
