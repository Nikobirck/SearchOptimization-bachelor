import re, nltk
from flow_framework import *
from metrics import top_k_accuracy
import keras
from snippet_generator import SnippetGenerator
from search_flow_components import CreateCui2docMappingFlowComponent, LoadClass2IdFlowComponent, PrintNumTokensFlowComponent, BordingJoinSummaryAndTextFlowComponent


class OnlyKeepListOfTokensFlowComponent(AbstractTransformerFlowComponent):

    def __init__(self, target_chars):
        self.target_chars = target_chars

    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                sample['text'] = [t for t in sample['text'] if t in self.target_chars]
                yield sample
        return new_generator

    def execute(self, environment={}):
        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        return environment

class ProbPhrases(object):
    def __init__(self, corpus):
        self.char2next = {}
        self.char2idx = {}
        dict_size = 1000
        for t in corpus:
            for i in range(0, len(t)-1):
                self.char2idx.setdefault(t[i+1], len(self.char2idx))
                self.char2next.setdefault(t[i], np.zeros((dict_size,1)))
                self.char2next[t[i]][self.char2idx[t[i+1]]] += 1
        for c in self.char2next:
            self.char2next[c] /= np.sum(self.char2next[c])

    def __getitem__(self, text):
        new_text = []
        current_idx = 0
        while (current_idx+1) < len(text):
            newx_token_prob = self.char2next[text[current_idx]][self.char2idx[text[current_idx + 1]]]
            if newx_token_prob > 10/len(self.char2idx):
                new_text.append(text[current_idx]+'_'+text[current_idx+1])
                current_idx += 2
            else:
                new_text.append(text[current_idx])
                current_idx += 1
        if current_idx+1 == len(text):
            new_text.append(text[-1])
        return  new_text


class ProbPhrases2(object):
    def __init__(self, corpus, max_n_gram=6):
        self.max_n_gram = max_n_gram
        self.ngram2count = [{} for i in range(0, max_n_gram-1)]
        for t in corpus:
            for n in range(2, max_n_gram):
                ngrams = nltk.ngrams(t, n)
                for ngram in ngrams:
                    self.ngram2count[n-2].setdefault(ngram, 0)
                    self.ngram2count[n - 2][ngram] +=1
        for n in range(2, max_n_gram):
            counts = self.ngram2count[n-2]
            avrg_count = np.percentile(list(counts.values()), 99.9)
            self.ngram2count[n-2] = dict([(ngram, counts[ngram]) for ngram in counts if counts[ngram]>(avrg_count)])


    def __getitem__(self, text):
        new_text = []
        for n in range(2, self.max_n_gram):
            ngrams = nltk.ngrams(text, n)
            all_n = {}
            for ngram in ngrams:
                if ngram in self.ngram2count[n-2]:
                    all_n[ngram] = self.ngram2count[n-2][ngram]
            print (sorted(all_n.items(), key=lambda x: x[1]))
        return  new_text


class ProbNgramFlowComponent(AbstractTransformerFlowComponent):
    def transform(self, text, env=None):
        for c in self.replacement_chars:
            text = text.replace(c, ' ')
        return text

    def generator_generator(self, it):
        def new_generator():
            for sample in it():
                sample['text'] = self.transform(sample['text'])
                yield sample
        return new_generator

    def execute(self, environment={}):
        environment['raw_train_samples_gen'] = self.generator_generator(environment['raw_train_samples_gen'])
        return environment

if __name__ == '__main__':

    metrics = ['accuracy',top_k_accuracy(5),top_k_accuracy(10),top_k_accuracy(20)]
    loss = 'sparse_categorical_crossentropy'
    optimizer = keras.optimizers.Adam(lr=0.001, clipnorm=6.)


    models = []

    # for token_type in ['unigram', 'n_gram']:
    #     for word_or_bucket_count in [1000, 2500, 5000, 10000, 15000, 20000]:
    #         models.append(('configs.english.hash_emb', -1, token_type, word_or_bucket_count))
    #         models.append(('configs.english.std_emb', word_or_bucket_count, token_type, -1))

    models.append( ('configs.english.hash_emb_dropout', -1, 'char', 20000))

    for model_name, max_words, cache_name, word_or_bucket_count in models:
        # provider = FlowController([ConfigLoaderFlowComponent('config.ini'), ExtendedFindzebraProvider()])
        provider = FlowController([ConfigLoaderFlowComponent('bording_config.ini'), BordingProvider()])

        config_spec = '%s_%i_%s_%i' % (model_name, abs(max_words), cache_name, abs(word_or_bucket_count))
        final_log = config_spec + '.txt'
        if os.path.isfile(os.path.join('results', final_log)):
            continue

        # weights_name = model_name
        checkpoint_path = 'checkpoints/{}'.format(config_spec)
        model_dir = 'final_models/{}'.format(config_spec)

        num_buckets_flow = []
        if word_or_bucket_count > 0:
            num_buckets_flow = [SetConfigValFlowComponent(['model', 'num_buckets'], word_or_bucket_count)]
        num_buckets_flow_controller = FlowController(num_buckets_flow)


        max_words_flow = []
        if max_words > 0:
            max_words_flow = [SetConfigValFlowComponent(['tokens', 'max_tokens'],max_words), MaxWordsFlowComponent()]
        max_words_flowcontroler = FlowController(max_words_flow)

        ngram_flow = []
        if not (cache_name == 'unigram'):
            ngram_flow = [NGramFlowComponent('bigram.pkl', cache_name, {'threshold':0.00001}), NGramFlowComponent('trigram.pkl', cache_name, {'threshold':0.001}), NGramFlowComponent('tetragram.pkl', cache_name, {'threshold':0.01}), NGramFlowComponent('pentagram.pkl', cache_name, {'threshold':0.1})]
        ngram_flowcontroler = FlowController(ngram_flow)

        start_flow = [
            provider]


        import string
        char2idx = dict([(c,i) for i,c in enumerate(string.printable)])

        flow = [
            FlowController(start_flow),
            CharTokenizeFlowComponent(),
            OnlyKeepListOfTokensFlowComponent(char2idx)
            ]

        def gen_str(c, char2next, char2idx):
            idx2char = dict([(i,c) for c, i in char2idx.items()])
            s = ''+c
            for i in range(0,100):
                n = char2next[c]
                i = np.random.choice(np.asarray(range(0, len(idx2char))), p=(n / sum(n)).reshape((-1,)))
                c = idx2char[i]
                s += c
            print (s)
        environment = FlowController(flow_components=flow).execute({})
        corpus = []
        for i, w in enumerate(environment['raw_train_samples_gen']()):
            corpus.append(w['text'])
            if i > 1000:
                break
        p = ProbPhrases2(corpus)
        p[corpus[0]]
        c2 = [p[c] for c in corpus]
        p2 = ProbPhrases(c2)
        c3 = [p2[c] for c in c2]
        p3 = ProbPhrases(c3)
        print(p3[p2[p['disease is baad']]])

        char2next = {}
        for w in environment['raw_train_samples_gen']():
            t = w['text']
            for i in range(0, len(t)-1):
                char2next.setdefault(t[i], np.zeros((len(char2idx),1)))
                char2next[t[i]][char2idx[t[i+1]]] += 1

        idx2char = dict([(i, c) for c, i in char2idx.items()])
        gen_str('A', char2next, char2idx)
        counter = 0
        for c in char2next:
            n = char2next[c]
            p = (n/sum(n)).reshape((-1,)).tolist()
            for idx, prob in enumerate(p):
                if prob > (1/float(len(char2next))):
                    counter += 1
                    print ('%s --> %s : %1.3f' % (c, idx2char[idx], prob))
        print(counter)



        environment = None