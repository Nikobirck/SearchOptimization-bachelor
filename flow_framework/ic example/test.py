from flow_framework import Predictor

p = Predictor('saved_model')
p.load_model()
cui2prefered_name = p.environment['cui2prefered_name']
prediction = [(cui2prefered_name[cui][0], prob) for cui, prob in p.predict('fever, headache',num_predictions=5)]
print (prediction)
print (p.predict('fever, headache',num_predictions=5))
print (p.predict('abdominal pain, sweating',num_predictions=5))
